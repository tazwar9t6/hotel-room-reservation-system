<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    protected $fillable = array('emp_name','staff_type_id','shift_id','id_card_type','id_card_no','address','contact_no','salary','joining_date');
}
