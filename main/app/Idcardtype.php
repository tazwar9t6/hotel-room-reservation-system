<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idcardtype extends Model
{
    protected $table = 'id_card_type';
    protected $fillable = array( 'id','id_card_type');
}
