<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Hash;
use App\Idcardtype;
use App\Roomtype;
use App\Customer;
use App\BookingReq;
use Session;

class ReservationController extends Controller
{
	
	
		
	public function Reservation()
    {
		$room_types = Roomtype::all();
		$idcards = Idcardtype::all();
		return view('reservation', compact('room_types','idcards'));
    }
	
	public function Getperson(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);

		$person = Roomtype::where('id',$request->id)->first();
		return response()->view('ajax.person', compact('person'));
    }
	
	public function Getprice(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
		
		$money = Roomtype::where('id',$request->id)->first();
		return response()->view('ajax.price', compact('money'));
    }
	
	public function Booking(Request $request)
    {
		if($request->customer == "new")
		{
		$this->validate($request,
            [
                'room_type_id' => 'required|numeric',
				'check_in' => 'required',
				'check_out' => 'required',
				'name' => 'required',
				'contact_no' => 'required|numeric|unique:customer,contact_no',
				'email' => 'required',
				'id_card_type' => 'required',
				'id_card_no' => 'required',
				'address' => 'required',
            ]);
	    $cus['customer_name'] = $request->name;
	    $cus['contact_no'] = $request->contact_no;
	    $cus['email'] = $request->email;
	    $cus['id_card_type_id'] = $request->id_card_type;
	    $cus['id_card_no'] = $request->id_card_no;
	    $cus['address'] = $request->address;
	    $id = Customer::create($cus);
		}
		if($request->customer == "old")
		{
			$this->validate($request,
            [
				'contact_no' => 'required|numeric'
            ]);
			
			if (Customer::where('contact_no', '=', $request->contact_no)->exists()) {
				$id = Customer::where('contact_no', '=', $request->contact_no)->first();
			}
			else
			{
				return response()->json(json_decode("{\"request\":\"failed.\",\"errors\":{\"contact_no\":[\"Provided contact number does not found.\"]}}", true), 401);
			}
		}
	   
	   
	  
	  $booking['customer_id'] = $id->id;
	  $booking['room_type'] = $request->room_type_id;
	  $booking['check_in'] = $request->check_in;
	  $booking['check_out'] = $request->check_out;
	  $booking['status'] = 1;
	  BookingReq::create($booking);

	  return response()->json([
                "status" => "Success"], 200);
	 
    }
}