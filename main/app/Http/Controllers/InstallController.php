<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Hash;
use App\Admin;
use Session;

class InstallController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('ifinstalled');
    }
	
	public function Index()
    {
       $extensions = [ 'openssl' ,'pdo', 'mbstring', 'tokenizer', 'JSON', 'cURL', 'XML', 'fileinfo' ];
	   $folders = [ 'main/bootstrap/cache/', 'main/storage/', 'main/storage/app/', 'main/storage/framework/', 'main/storage/logs/'];
	   return view('install.index', compact('extensions','folders')); 
    }
	public function DBForm()
    {
		return view('install.dbform');
    }
	public function AdminForm()
    {
		if(Session::get('DBInfo') == true){
		return view('install.admin'); 
		}
		else{
			return redirect()->route('installer');
		}
    }
	
	public function DBInfo(Request $request)
    {
		$this->validate($request,
            [
				'db_host' => 'required',
				'db_user' => 'required',
				'db_name' => 'required',
            ]);
	   
	   $dbcon = @mysqli_connect($request->db_host, $request->db_user, $request->db_password, $request->db_name);
	   if (mysqli_connect_errno())
	   {
	   return back()->with('alert', mysqli_connect_error());
	   }
	   if($dbcon == true)
	   {
		   $this->setEnvironmentValue("DB_HOST", $request->db_host);
		   $this->setEnvironmentValue("DB_DATABASE", $request->db_name);
		   $this->setEnvironmentValue("DB_USERNAME", $request->db_user);
		   $this->setEnvironmentValue("DB_PASSWORD", $request->db_password);
		   $this->setEnvironmentValue("APP_URL",url('/'));
    	   $check = DBQuery($request->db_host, $request->db_name, $request->db_user, $request->db_password);
		   if($check == true ){
			   Session::put('DBInfo', true);
		   return redirect()->route('AdminForm');
		   }
		   else {
			   return back()->with('alert', "Unknown error. Please contact with developer.");
		   }
	   }
    }
	
	
	public function AdminSetup(Request $request)
    {
		$this->validate($request,
            [
				'admin_name' => 'required',
				'admin_username' => 'required',
				'admin_password' => 'required',
				'admin_email' => 'required',
				'timezone' => 'required',
            ]);
		
	  $user['name'] = $request->admin_name;
	  $user['username'] = $request->admin_username;
	  $user['email'] = $request->admin_email;
	  $user['password'] = bcrypt($request->admin_password);
	  $user['type'] = "admin";
	  
	  Admin::create($user);
	  $this->setEnvironmentValue("APP_ENV", "production");
	  $this->setEnvironmentValue("APP_TIMEZONE", $request->timezone);
	  return back()->with('success', "Installation Complete.");
	}
	
	public function Verify(){
		//return view('install.verify'); 
	}
	
	public function VerificationChecker(Request $request){
		
		//
	}
	
	public function setEnvironmentValue(string $key, string $value, $env_path = null)
	{
		$value = preg_replace('/\s+/', '', $value);
		$key = strtoupper($key);
		$env = file_get_contents(isset($env_path) ? $env_path : base_path('.env'));
		$env = str_replace("$key=" . env($key), "$key=" . $value, $env);
		$env = file_put_contents(isset($env_path) ? $env_path : base_path('.env'), $env);
	}
	
	public function HTTP($code,$id){
	
	//
	
	}
	
	
}