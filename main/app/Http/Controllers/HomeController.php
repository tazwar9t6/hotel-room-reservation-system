<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Auth;
use App\Room;
use App\Roomtype;
use App\Staff;
use App\Customer;
use App\Idcardtype;
use App\Booking;
use App\Shift;
use App\Smtp;
use App\StaffHistory;
use App\Stafftype;
use App\Company;
use App\BookingReq;
use DateTime;
use Hash;

class HomeController extends Controller
{
   public function __construct()
    {
        $this->middleware('admin');
    }
	
    public function Home()
    {
       $admin = Auth::guard('admin')->user();
	   $rooms = Room::all()->count();
	   $latests = Booking::limit(20)->orderBy('created_at','desc')->get();
	   $activeroom = Room::where('status',0)->count();
	   $request = BookingReq::where('status',1)->count();
	   $customers = Customer::limit(20)->orderBy('created_at','desc')->get();
	   $staff = Staff::all()->count();
        return view('admin.home', compact('admin','rooms','latests','activeroom','request','customers','staff'));
    }
	
	public function SysUsers()
    {
		$admin = Auth::guard('admin')->user();
		$admins = Admin::orderBy('updated_at','desc')->get();
		if($admin->type == "admin"){
		return view('admin.sysusers', compact('admin','admins'));
		}
		if($admin->type == "manager"){
			$admins = Admin::where('id', $admin->id)->get();
		return view('admin.sysusers', compact('admin','admins'));
		}
    }
	
	public function SysChngPass(Request $request)
    {
		$this->validate($request,
            [
				'id' => 'required',
            ]);
	  $admin = Auth::guard('admin')->user();
	  if($admin->type == "admin"){
		  if($admin->id == $request->id){
			  $this->validate($request,
            [
				'pass' => 'required',
				'old_pass' => 'required|string|min:6',
            ]);
			
			if (Hash::check($request->old_pass, $admin->password)) { 
			 $user = Admin::find($request->id);
			 $user['password'] = Hash::make($request->pass);
			 $user->save();
			 return back()->with('success', 'Password Changed.');
			}
			else{
				return back()->with('alert', 'Old Password doesn\'t match.');
			}
		  
	  }
	  else{
		  $this->validate($request,
            [
				'pass' => 'required|string|min:6',
            ]);
			
			$user = Admin::find($request->id);
			$user['password'] = Hash::make($request->pass);
			$user->save();
			return back()->with('success', 'Password Changed.');
	  }
	  }
	  if($admin->type == "manager"){
			  $this->validate($request,
            [
				'pass' => 'required|string|min:6',
				'old_pass' => 'required',
            ]);
			
			if (Hash::check($request->old_pass, $admin->password)) { 
			 $user = Admin::find($admin->id);
			 $user['password'] = Hash::make($request->pass);
			 $user->save();
			 return back()->with('success', 'Password Changed.');
			}
		  else{
				return back()->with('alert', 'Old Password doesn\'t match.');
			}
	  }
	  
    }

	public function EditSysUser(Request $request)
    {
		$this->validate($request,
            [
				'id' => 'required',
				'name' => 'required',
				'email' => 'required|email',
            ]);
	  $admin = Auth::guard('admin')->user();
	  if($admin->type == "admin"){
		  
   	    $user = Admin::find($request->id);
		$user['name'] = $request->name;
		$user['email'] = $request->email;
		$user->save();
		return back()->with('success', 'User Data Edited.');
		}
		
	  if($admin->type == "manager"){
		  
   	    $user = Admin::where('id', $admin->id)->first();
		$user['name'] = $request->name;
	    $user['email'] = $request->email;
		$user->save();
		return back()->with('success', 'Data Edited.');
		}
		
		
		
		
    }
	
	public function Allrooms()
    {
		$admin = Auth::guard('admin')->user();
		$rooms = Room::orderBy('updated_at','desc')->get();
		return view('admin.allroom', compact('admin','rooms'));
    }
	
	public function Allstaff()
    {
		$admin = Auth::guard('admin')->user();
		$staffs = Staff::all();
		return view('admin.allstaff', compact('admin','staffs'));
    }
	
	public function AllCus()
    {
		$admin = Auth::guard('admin')->user();
		$customers = Booking::orderBy('created_at','desc')->get();
		return view('admin.allcustomer', compact('admin','customers'));
    }
	
	public function IdCards()
    {
		$admin = Auth::guard('admin')->user();
		$cards = Idcardtype::all();
		return view('admin.idcards', compact('admin','cards'));
    }
	
	public function allShift()
    {
		$admin = Auth::guard('admin')->user();
		$shifts = Shift::all();
		return view('admin.allshift', compact('admin','shifts'));
    }
	
	public function staffPosition()
    {
		$admin = Auth::guard('admin')->user();
		$types = Stafftype::all();
		return view('admin.staff', compact('admin','types'));
    }
	
	public function Roomtypes()
    {
		$admin = Auth::guard('admin')->user();
		$types = Roomtype::all();
		return view('admin.roomtypes', compact('admin','types'));
    }
	
	public function Reservation()
    {
		$admin = Auth::guard('admin')->user();
		$room_types = Roomtype::all();
		$idcards = Idcardtype::all();
		return view('admin.reservation', compact('admin','room_types','idcards'));
    }
	
	public function Reservation3($id,$typeid,$customer)
    {
		$admin = Auth::guard('admin')->user();
		$room_types = Roomtype::all();
		$idcards = Idcardtype::all();
		$data = Customer::where('contact_no', $customer)->first();
		$booking = BookingReq::where('id', $id)->first();
		$staying_day = round((strtotime($booking->check_out) - strtotime($booking->check_in)) / (60 * 60 * 24));

		return view('admin.reservationonreq', compact('admin','room_types','idcards', 'data', 'typeid', 'booking', 'staying_day'));
    }
	
	public function ReservationReq()
    {
		$admin = Auth::guard('admin')->user();
		$requests = BookingReq::where('status', 1)->orderBy('created_at', 'desc')->get();
		return view('admin.reservationreq', compact('admin','requests'));
    }
	
	public function Reservation2($type,$room)
    {
		$admin = Auth::guard('admin')->user();
		$room_type = Roomtype::where('id',$type)->first();
		if($room_type == null){
		$rooms = Room::all();
		return view('admin.allroom', compact('admin','rooms'));
		}
		$room = Room::where('id',$room)->first();
		if($room == null){
		$rooms = Room::all();
		return view('admin.allroom', compact('admin','rooms'));
		}
		$idcards = Idcardtype::all();
		return view('admin.custom_reservation', compact('admin','room_type','room','idcards'));
    }
	
	public function EmpHistory($id)
    {
		$admin = Auth::guard('admin')->user();
		$staffs = Staff::where('id',$id)->first();
		if($staffs == null){
		$staffs = Staff::all();
		return view('admin.allstaff', compact('admin','staffs'));
		}
		$histories = StaffHistory::where('emp_id',$id)->orderBy('created_at','desc')->get();
		return view('admin.staffhistory', compact('admin','staffs','histories'));
    }
	
	public function Getrooms(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);

		$rooms = Room::where('room_type_id',$request->id)->where('status',0)->get();
		return response()->view('admin.ajax.getrooms', compact('rooms'));
    }
	
	public function Getprice(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
		
		$room = Room::where('id',$request->id)->first();
		$money = Roomtype::where('id',$room->room_type_id)->first();
		return response()->view('admin.ajax.price', compact('money'));
    }
	
	public function checkInRoom(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
		$booking = Booking::where('room_id',$request->id)->where('payment_status',0)->first();
		$customer = Customer::where('id',$booking->customer_id)->first();
		$room = Room::where('id',$booking->room_id)->first();
		$type = Roomtype::where('id',$room->room_type_id)->first();
		$today = date('d-m-Y');
		if ($today == $booking->check_in){
			$checkIn = date('M j, Y', strtotime($booking->check_in));
		}
		else {
			$checkIn = date('M j, Y', strtotime($booking->check_in)) . " <a id='checkIntoday' data-id='{$booking->id}' class='btn btn-primary btn-xs m-r-5'>Check In Now</a>";
		}
		
		return response()->json([
                "status" => "Success",
				"name" => ucwords($customer->customer_name),
				"room_type" => $type->room_type,
				"room_no" => $room->room_no,
				"room_price" => $type->price,
				"check_in" => $checkIn,
				"check_out" => date('M j, Y', strtotime($booking->check_out)),
				"total_price" => $booking->total_price,
				"booking_id" => $booking->id ], 200);
    }
	
	public function checkOutRoom(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
		$booking = Booking::where('room_id',$request->id)->where('payment_status',0)->first();
		$customer = Customer::where('id',$booking->customer_id)->first();
		$room = Room::where('id',$booking->room_id)->first();
		$type = Roomtype::where('id',$room->room_type_id)->first();
		$today = date('d-m-Y');
		$timestamp = strtotime($booking->check_out);
		$date = date('d-m-Y', $timestamp);
		$checkOut = $booking->check_out == null ? "<a id='checkOuttoday' data-id='{$booking->id}' class='btn btn-primary btn-xs m-r-5'>Check Out Now</a>" : date('M j, Y', strtotime($booking->check_out)) . " <a id='checkOuttoday' data-id='{$booking->id}' class='btn btn-primary btn-xs m-r-5'>Check Out Now</a>";
		if ($booking->remaining_price < 0)
		{
	    	$remaining = "<a class='btn btn-primary btn-xs m-r-5'>Return ". abs($booking->remaining_price)." to customer</a>";
			}
		else{
			$remaining = $booking->remaining_price;
		}
		
		$today = date('M j, Y g:i A', time());
		$dtF = new DateTime($booking->check_in);
		$dtT = new DateTime($today);
		$day = $dtF->diff($dtT)->format('%a');
		$hour = $dtF->diff($dtT)->format('%h');
		return response()->json([
                "status" => "Success",
				"name" => ucwords($customer->customer_name),
				"room_type" => $type->room_type,
				"room_no" => $room->room_no,
				"room_price" => $type->price,
				"check_in" => date('M j, Y - g:i A', strtotime($booking->check_in)),
				"check_out" => $checkOut,
				"total_price" => $booking->total_price,
				"paid" => $booking->advance,
				"remaining_price" => $remaining,
				"day" => $day,
				"hour" => $hour,
				"booking_id" => $booking->id ], 200);
    }
	
	public function checkIntoday(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
		$booking = Booking::where('id',$request->id)->where('payment_status',0)->first();
		$room = Room::where('id',$booking->room_id)->first();
		$type = Roomtype::where('id',$room->room_type_id)->first();
		$today = date('d-m-Y')." ".date("H:i:s");
		$date1 = date_create($today);
		$date2 = date_create($booking->check_out);
		$diff = date_diff($date1,$date2);
		$diff = $diff->format("%r%a")+1;
		$total_price = $type->price*$diff;
		$booking['check_in'] = $today;
		$booking['total_price'] = $total_price;
		$booking->save();
		return response()->json([
				"check_in" => date('M j, Y', strtotime($booking->check_in)),
				"total_price" => $booking->total_price
				], 200);
    }
	
	public function checkOutBooking(Request $request)
    {
		$this->validate($request,
            [
                'booking_id' => 'required',
				'remaining_amount' => 'required',
            ]);
		$booking = Booking::where('id',$request->booking_id)->where('payment_status',0)->first();
		if($request->remaining_amount == null){
		$remaining_price = 0;
		} else {
			$remaining_price = $booking['remaining_price'] - $request->remaining_amount;
		}
			$booking['check_out'] = date('d-m-Y')." ".date("H:i:s");
//			$booking['advance'] = 0;
			$booking['remaining_price'] = $remaining_price;
			$booking['payment_status'] = 1;
			$booking->save();
			$room = Room::where('id',$booking->room_id)->first();
			$room['status'] = 0;
			$room['check_in_status'] = 0;
			$room['check_out_status'] = 1;
			$room->save();
			$url = route('showInvoice1', [$booking->id]);
			return response()->json([
				"status" => "Success",
				"msg" => "Successfully checked out.",
				"url" => $url,
				], 200);	
    }
	
	public function checkOuttoday(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
		$booking = Booking::where('id',$request->id)->where('payment_status',0)->first();
		$room = Room::where('id',$booking->room_id)->first();
		$type = Roomtype::where('id',$room->room_type_id)->first();

		$advance = $booking->advance;

		$today = date('d-m-Y')." ".date("H:i:s");
		$dtF = new DateTime($booking->check_in);
		$dtT = new DateTime($today);
		$day = $dtF->diff($dtT)->format('%a');
		$hour = $dtF->diff($dtT)->format('%h');
		
		if($day == 0 && $hour > 0){
			$total_price = $type->price;
		}
		elseif($day > 0 && $hour == 0){
			$total_price = $type->price*$day;
		}
		elseif( $day == 0 || $hour == 0){
			$total_price = $type->price;
		}
		elseif( $day > 0 && $hour > 0){
			$total_price = $type->price*($day+1);			
			}
		
		$booking['check_out'] = date('d-m-Y')." ".date("H:i:s");
		$booking['total_price'] = $total_price;
		$booking['remaining_price'] = $total_price - $advance;
		$booking->save();
		if ($booking->remaining_price < 0)
		{
	    	$remaining = "<a class='btn btn-primary btn-xs m-r-5'>Return ". abs($booking->remaining_price)." to customer</a>";
			}
		else{
			$remaining = $booking->remaining_price;
		}
		return response()->json([
				"check_out" => date('M j, Y - g:i A', strtotime($booking->check_out)),
				"total_price" => $booking->total_price,
				"remaining_price" => $remaining,
				"day" => $day,
				"hour" => $hour
				], 200);
    }
	
	public function advancePayment(Request $request)
    {
		$this->validate($request,
            [
			   'booking_id' => 'required',
			   ]);
	    if ($request->advance_payment == null){
			$advance = 0;
		}
		else{
			$advance = $request->advance_payment;
		}
		$booking = Booking::where('id',$request->booking_id)->first();
		$remaining_price = $booking->total_price - $advance;
		
		$booking = Booking::find($request->booking_id);
		$booking['check_in'] = date('d-m-Y')." ".date("H:i:s");
		$booking['advance'] = $advance;
		$booking['remaining_price'] = $remaining_price;
		$booking->save();
		$room = Room::find($booking->room_id);
		$room['check_in_status'] = 1;
		$room->save();
		return response()->json([
                "status" => "Success"], 200);	
	}
	
	public function cutomerDetails(Request $request)
    {
		$this->validate($request,
            [
			   'room_id' => 'required',
			   ]);
		$booking = Booking::where('room_id',$request->room_id)->orderBy('created_at','desc')->first();
		$customer = Customer::where('id',$booking->customer_id)->first();
		$idcard = Idcardtype::where('id',$customer->id_card_type_id)->first();
		return response()->json([
                "status" => "Success",
				"customer_name" => ucwords($customer->customer_name),
				"contact_no" => $customer->contact_no,
				"email" => $customer->email,
				"id_card_type" => $idcard->id_card_type,
				"id_card_no" => $customer->id_card_no,
				"address" => $customer->address,
				"book_date" => date('M j, Y - g:i A', strtotime($booking->booking_date)),
				"checked_in" => $booking->check_in == null ? "" : date('M j, Y - g:i A', strtotime($booking->check_in)),
				"remaining_price" => $booking->remaining_price
				], 200);	
	}
	
	public function bookingDetails(Request $request)
    {
		$this->validate($request,
            [
			   'id' => 'required',
			   ]);
		$booking = Booking::where('customer_id',$request->id)->first();
		if($booking->remaining_price == 0){
			$status = "<a class='btn btn-primary btn-xs m-r-5'>Complete</a>";
		}
		else{
			$status = "<a class='btn btn-primary btn-xs m-r-5'>Due ". abs($booking->remaining_price)."</a>";
		}
		return response()->json([
                "status" => "Success",
				"customer_name" => ucwords($booking->Customer->customer_name),
				"contact_no" => $booking->Customer->contact_no,
				"email" => $booking->Customer->email,
				"id_card_type" => $booking->Customer->Idcardtype->id_card_type,
				"id_card_no" => $booking->Customer->id_card_no,
				"address" => $booking->Customer->address,
				"check_id" => date('M j, Y - g:i A', strtotime($booking->check_in)),
				"check_out" => date('M j, Y - g:i A', strtotime($booking->check_out)),
				"total" => $booking->total_price,
				"advance" => $booking->advance,
				"payment_status" => $status
				], 200);	
	}
	
	public function cusDetails(Request $request)
    {
		$this->validate($request,
            [
			   'id' => 'required',
			   ]);
		
		$bookrequest = BookingReq::where('customer_id', $request->id)->first();

		return response()->json([
                "status" => "Success",
				"customer_name" => ucwords($bookrequest->Customer->customer_name),
				"contact_no" => $bookrequest->Customer->contact_no,
				"email" => $bookrequest->Customer->email,
				"id_card_type" => $bookrequest->Customer->Idcardtype->id_card_type,
				"id_card_no" => $bookrequest->Customer->id_card_no,
				"address" => $bookrequest->Customer->address,
				"booking_date" => date('M j, Y - g:i A', strtotime($bookrequest->booking_date)),
				"check_id" => date('M j, Y', strtotime($bookrequest->check_in)),
				"check_out" => date('M j, Y', strtotime($bookrequest->check_out))
				], 200);	
	}
	
	public function Booking(Request $request)
    {
		
		$this->validate($request,
            [
                'room_type_id' => 'required|numeric',
				'room_id' => 'required|numeric',
				'check_in' => 'required',
				'check_out' => 'required',
				'total_price' => 'required|numeric',
				'name' => 'required',
				'contact_no' => 'required|numeric',
				'email' => 'required',
				'id_card_type' => 'required',
				'id_card_no' => 'required',
				'address' => 'required',
            ]);
			
	  if (room::where('id', '=', $request->room_id)->where('status',1)->exists()) {
	  return response()->json([
                "status" => "Failed",
				"msg" => "Room already booked."], 422);
	  }
      else {
      if (Customer::where('contact_no', '=', $request->contact_no)->exists()) {
				$id = Customer::where('contact_no', '=', $request->contact_no)->first();
			}
	  else
	  {
	  $cus['customer_name'] = $request->name;
	  $cus['contact_no'] = $request->contact_no;
	  $cus['email'] = $request->email;
	  $cus['id_card_type_id'] = $request->id_card_type;
	  $cus['id_card_no'] = $request->id_card_no;
	  $cus['address'] = $request->address;
	  $id = Customer::create($cus);
	  }
	  $booking['customer_id'] = $id->id;
	  $booking['room_id'] = $request->room_id;
	  $booking['check_in'] = $request->check_in;
	  $booking['check_out'] = $request->check_out;
	  $booking['total_price'] = $request->total_price;
	  $booking['advance'] = 0;
	  $booking['remaining_price'] = $request->total_price;
	  $booking['payment_status'] = 0;
	  Booking::create($booking);
	  $room = Room::find($request->room_id);
	  $room['status'] = 1;
	  $room->save();
	  return response()->json([
                "status" => "Success",
				"message" => "New room booked for new Guest."], 200);
	  }
    }
	
	public function Booking1(Request $request, $bookingreqid)
    {
		
		$this->validate($request,
            [
                'room_type_id' => 'required|numeric',
				'room_id' => 'required|numeric',
				'check_in' => 'required',
				'check_out' => 'required',
				'total_price' => 'required|numeric',
				'name' => 'required',
				'contact_no' => 'required|numeric',
				'email' => 'required',
				'id_card_type' => 'required',
				'id_card_no' => 'required',
				'address' => 'required',
            ]);
			
	  if (room::where('id', '=', $request->room_id)->where('status',1)->exists()) {
	  return response()->json([
                "status" => "Failed",
				"msg" => "Room already booked."], 422);
	  }
      else {	  
	  $cus = Customer::where('contact_no', '=', $request->contact_no)->first();
	  $cus['customer_name'] = $request->name;
	  $cus['email'] = $request->email;
	  $cus['id_card_type_id'] = $request->id_card_type;
	  $cus['id_card_no'] = $request->id_card_no;
	  $cus['address'] = $request->address;
	  $cus->save();
	  $booking['customer_id'] = $cus->id;
	  $booking['room_id'] = $request->room_id;
	  $booking['check_in'] = $request->check_in;
	  $booking['check_out'] = $request->check_out;
	  $booking['total_price'] = $request->total_price;
	  $booking['advance'] = 0;
	  $booking['remaining_price'] = $request->total_price;
	  $booking['payment_status'] = 0;
	  Booking::create($booking);
	  $room = Room::find($request->room_id);
	  $room['status'] = 1;
	  $room->save();
	  $bookingreq = BookingReq::find($bookingreqid);
	  $bookingreq['status'] = 0;
	  $bookingreq->save();
	  return response()->json([
                "status" => "Success",
				"message" => "New room booked for new Guest."], 200);
	  }
    }
	
	public function RemoveRoomtypes(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Roomtype::where('id', '=', $request->id)->exists()) {
	  Roomtype::where('id',$request->id)->delete();
	  return back()->with('success', 'Room type removed.');
	  }
    }
	
	public function ReservationReqRemove(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (BookingReq::where('id', '=', $request->id)->exists()) {
	  BookingReq::where('id',$request->id)->delete();
	  return back()->with('success', 'request removed.');
	  }
    }
	
	public function Editstafftype(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
				'position' => 'required',
            ]);
	  if (Stafftype::where('staff_type', '=', $request->position)->exists()) {
	  return back()->with('alert', 'Staff Position exists.');
	  }
	  $type = Stafftype::find($request->id);
	  $type['staff_type'] = $request->position;
	  $type->save();
	  return back()->with('success', 'Staff Position edited.');
    }
	
	public function EditIdcard(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
				'name' => 'required',
            ]);
	  if (Idcardtype::where('id_card_type', '=', $request->name)->exists()) {
	  return back()->with('alert', 'IdCard type exists.');
	  }
	  $type = Idcardtype::find($request->id);
	  $type['id_card_type'] = $request->name;
	  $type->save();
	  return back()->with('success', 'IdCard type edited.');
    }
	
	public function editShift(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
				'shift' => 'required',
				'timing' => 'required',
            ]);
	  if (Shift::where('shift_timing', '=', $request->timing)->exists()) {
	  return back()->with('alert', 'Shift Timing exists.');
	  }
	  $shift = Shift::find($request->id);
	  $shift['shift'] = $request->shift;
	  $shift['shift_timing'] = $request->timing;
	  $shift->save();
	  return back()->with('success', 'Shift edited.');
    }
	
	public function RemoveRoom(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Room::where('id', '=', $request->id)->exists()) {
	  Room::where('id',$request->id)->delete();
	  return back()->with('success', 'Room type removed.');
	  }
    }
	
	public function removeShift(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Shift::where('id', '=', $request->id)->exists()) {
	  Shift::where('id',$request->id)->delete();
	  return back()->with('success', 'Shift removed.');
	  }
    }
	
	public function removeCus(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Customer::where('id', '=', $request->id)->exists()) {
	  Customer::where('id',$request->id)->delete();
	  return back()->with('success', 'Customer removed.');
	  }
    }
	
	public function RemovePosition(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Stafftype::where('id', '=', $request->id)->exists()) {
	  Stafftype::where('id',$request->id)->delete();
	  return back()->with('success', 'Staff Position removed.');
	  }
    }
	
	public function RemoveSysUser(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Admin::where('id', '=', $request->id)->exists()) {
	  Admin::where('id',$request->id)->delete();
	  return back()->with('success', 'System User removed.');
	  }
    }
	
	public function RemoveIdCard(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Idcardtype::where('id', '=', $request->id)->exists()) {
	  Idcardtype::where('id',$request->id)->delete();
	  return back()->with('success', 'Id Card type removed.');
	  }
    }
	
	public function RemoveStaff(Request $request)
    {
		$this->validate($request,
            [
                'id' => 'required',
            ]);
	  if (Staff::where('id', '=', $request->id)->exists()) {
	  Staff::where('id',$request->id)->delete();
	  return back()->with('success', 'Staff removed.');
	  }
    }
	
	public function AddSysUser(Request $request)
    {
      $this->validate($request,
            [
                'name' => 'required',
                'user' => 'required',
				'pass' => 'required|string|min:6',
				'email' => 'required|email',
				'role' => 'required',
            ]);

	  if (Admin::where('username', '=', $request->user)->exists()) {
	  return back()->with('alert', 'System User exists.');
	  }
	  if($request->role == 1){
		  $role = "admin";
	  }
	  else {
		  $role = "manager";
	  }
	  $user['name'] = $request->name;
	  $user['username'] = $request->user;
	  $user['email'] = $request->email;
	  $user['password'] = bcrypt($request->pass);
	  $user['type'] = $role;
	  
	  Admin::create($user);

	  return back()->with('success', 'New System Users added.');
	
	}
	
	public function Addroomtype(Request $request)
    {
      $this->validate($request,
            [
                'roomtype' => 'required',
                'roomprice' => 'required',
				'maxperson' => 'required',
            ]);
	  if (Roomtype::where('room_type', '=', $request->roomtype)->exists()) {
	  return back()->with('alert', 'Room type exists.');
	  }
	  $room['room_type'] = $request->roomtype;
	  $room['price'] = $request->roomprice;
	  $room['max_person'] = $request->maxperson;
	  
	  Roomtype::create($room);

	  return back()->with('success', 'New room type added.');
	
	}
	
	public function AddIdCard(Request $request)
    {
      $this->validate($request,
            [
                'name' => 'required',
            ]);
	  if (Idcardtype::where('id_card_type', '=', $request->name)->exists()) {
	  return back()->with('alert', 'ID Card type exists.');
	  }
	  $card['id_card_type'] = $request->name;
	  
	  Idcardtype::create($card);

	  return back()->with('success', 'New IdCard type added.');
	
	}
	
	public function Editroom(Request $request)
    {
      $this->validate($request,
            [
			    'id' => 'required',
                'room_type_id' => 'required',
                'room_no' => 'required',
            ]);
	  $room = Room::find($request->id);
	  $room['room_type_id'] = $request->room_type_id;
	  $room['room_no'] = $request->room_no;
	  $room->save();
	  return back()->with('success', 'room edited.');
	}
	
	public function Editroomtype(Request $request)
    {
      $this->validate($request,
            [
			    'id' => 'required',
                'room_type' => 'required',
                'price' => 'required',
				'max_person' => 'required',
            ]);
	  $room = Roomtype::find($request->id);
	  $room['room_type'] = $request->room_type;
	  $room['price'] = $request->price;
	  $room['max_person'] = $request->max_person;
	  
	  $room->save();

	  return back()->with('success', 'room type edited.');
	
	}
	
	public function Addroom(Request $request)
    {
      $this->validate($request,
            [
                'roomtype' => 'required',
                'roomnumber' => 'required',
            ]);
	  if (Room::where('room_no', '=', $request->roomnumber)->exists()) {
	  return back()->with('alert', 'Room number exists.');
	  }
	  $room['room_type_id'] = $request->roomtype;
	  $room['room_no'] = $request->roomnumber;
	  $room['status'] = 0;
	  $room['check_in_status'] = 0;
	  $room['check_out_status'] = 0;
	  $room['deleteStatus'] = 0;
	  
	  Room::create($room);

	  return back()->with('success', 'New room number added.');
	
	}
	
	public function changeShift(Request $request)
    {
      $this->validate($request,
            [
			    'staff_id' => 'required',
                'shift_id' => 'required',
            ]);
	  $shift = Staff::find($request->staff_id);
	  $shift['shift_id'] = $request->shift_id;
	  $shift->save();
	  $emp_u = StaffHistory::where('emp_id', '=', $shift->id)->where('to_date',NULL)->first();
	  $emp_u['to_date'] = date("Y-m-d H:i:s");
	  $emp_u->save();
	  
	  $emp_h['emp_id'] = $shift->id;
	  $emp_h['shift_id'] = $request->shift_id;
	  StaffHistory::create($emp_h);
	  
	  return back()->with('success', 'Staff shift changed.');
	
	}
	
	
	public function AddEmp(Request $request)
    {
      $this->validate($request,
            [
                'position' => 'required',
                'shift' => 'required',
				'name' => 'required',
                'idcard' => 'required',
				'id_card_no' => 'required',
                'number' => 'required',
				'address' => 'required',
                'salary' => 'required',
            ]);

	  $emp['emp_name'] = $request->name;
	  $emp['staff_type_id'] = $request->position;
	  $emp['shift_id'] = $request->shift;
	  $emp['id_card_type'] = $request->idcard;
	  $emp['id_card_no'] = $request->id_card_no;
	  $emp['address'] = $request->address;
	  $emp['contact_no'] = $request->number;
	  $emp['salary'] = $request->salary;
	  $id = Staff::create($emp);
	  $emp_h['emp_id'] = $id->id;
	  $emp_h['shift_id'] = $request->shift;
	  StaffHistory::create($emp_h);
	  return back()->with('success', 'New staff added.');
	
	}
	
	public function editStaff(Request $request)
    {
      $this->validate($request,
            [
			    'id' => 'required',
                'position' => 'required',
                'shift' => 'required',
				'name' => 'required',
                'idcard' => 'required',
				'id_card_no' => 'required',
                'number' => 'required',
				'address' => 'required',
                'salary' => 'required',
            ]);
	  $emp = Staff::find($request->id);
	  $emp['emp_name'] = $request->name;
	  $emp['staff_type_id'] = $request->position;
	  $emp['shift_id'] = $request->shift;
	  $emp['id_card_type'] = $request->idcard;
	  $emp['id_card_no'] = $request->id_card_no;
	  $emp['address'] = $request->address;
	  $emp['contact_no'] = $request->number;
	  $emp['salary'] = $request->salary;
	  $emp->save();
	  return back()->with('success', 'Staff edited.');
	
	}
	
	public function addPositioin(Request $request)
    {
      $this->validate($request,
            [
                'name' => 'required',
            ]);
	  if (Stafftype::where('staff_type', '=', $request->name)->exists()) {
	  return back()->with('alert', 'Staff Position exists.');
	  }
	  $type['staff_type'] = $request->name;
	  $id = Stafftype::create($type);
	  return back()->with('success', 'New staff position added.');
	
	}
	
	public function addShift(Request $request)
    {
      $this->validate($request,
            [
                'timing' => 'required',
				"shift" => 'required',
            ]);
	  if (Shift::where('shift_timing', '=', $request->timing)->exists()) {
	  return back()->with('alert', 'Shift exists.');
	  }
	  $shift['shift'] = $request->shift;
	  $shift['shift_timing'] = $request->timing;

	  Shift::create($shift);
	  return back()->with('success', 'New shift added.');
	
	}
	
	public function showPDFInvoice($id)
    {
		return ShowPdf($id);
	}
	
	public function showInvoice1($id)
    {
		$admin = Auth::guard('admin')->user();
		$invoice = $id;
		return view('admin.pdf', compact('admin','invoice'));
	}
	
	public function Invoice()
    {
		$admin = Auth::guard('admin')->user();
		$invoice = Company::first();
		return view('admin.invoice', compact('admin','invoice'));
	}
	
	public function InvoiceUpdate(Request $request)
    {
		$this->validate($request,
            [
                'name' => 'required',
				'address' => 'required',
				'email' => 'required|email',
				'number' => 'required',
				'watermark' => 'required',
				'currency' => 'required',
				'footer' => 'required',
            ]);
		
		$invoice = Company::first();
		$invoice['name'] = $request->name;
		$invoice['address'] = $request->address;
		$invoice['email'] = $request->email;
		$invoice['phone'] = $request->number;
		$invoice['currency'] = $request->currency;
		$invoice['watermark'] = $request->watermark;
		$invoice['footer'] = $request->footer;
		$invoice->save();
		return back()->with('success', 'Invoice Data Updated.');
	}
	
}
