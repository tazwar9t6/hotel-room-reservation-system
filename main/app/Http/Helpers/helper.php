<?php

function extension_check($name)
{
	if (!extension_loaded($name))
	{
		$response = false;
		} else {
			$response = true;
			}
	return $response;
}

function folder_permission($name)
{
	$perm = substr(sprintf('%o', fileperms($name)), -4);
	if ($perm >= '0775')
	{
		return true;
		} else {
			return false;
			}
}

function CreateTable($name, $details, $status)
{
	if ($status=='1')
	{
		$pr = '<i class="fa fa-check" style="color:green;"><i>';
		}else{
			$pr = '<i class="fa fa-times" style="color:red;"><i>';
			}
			return "<tr><td>$name</td><td>$details</td><td>$pr</td></tr>";
}

function DBQuery($host,$db,$user,$pass)
{
	set_time_limit(0);
	$conn = @mysqli_connect($host,$user,$pass,$db);
	if ($conn){
		$templine = '';
		$lines = file("assets/database.sql");
		foreach ($lines as $line)
		{
			if (substr($line, 0, 2) == '--' || $line == '')
				continue;
			$templine .= $line;
			if (substr(trim($line), -1, 1) == ';')
			{
				mysqli_query($conn, $templine) or mysqli_error();
				$templine = '';
				}
		}
		return true;
	}
}



