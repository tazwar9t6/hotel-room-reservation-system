<?php
use App\Company;
use App\Booking;
use App\Customer;
use App\Room;
use App\Roomtype;

if (! function_exists('ShowPdf')) {
	
	function ShowPdf($id){
		$company = Company::first();
		$booking = Booking::where('id',$id)->first();
		if($booking == null){ return redirect()->route('home')->with('alert', 'Invalid Request.'); }
		$customer = Customer::where('id', $booking->customer_id)->first();
		$room = Room::where('id', $booking->room_id)->first();
		$roomtype = Roomtype::where('id', $room->room_type_id)->first();
		$booking_date = date('M j, Y - g:i A', strtotime($booking->booking_date));
		$check_in = date('M j, Y - g:i A', strtotime($booking->check_in));
		$check_out = date('M j, Y - g:i A', strtotime($booking->check_out));
		$dtF = new DateTime($booking->check_in);
		$dtT = new DateTime($booking->check_out);
		$day = $dtF->diff($dtT)->format('%a');
		$hour = $dtF->diff($dtT)->format('%h');
		if($day == 0)
		{
			$day = 1;
			}
	   elseif($day == 0 && $hour == 0)
	   {
		   $day = 1;
		}
	   
	   elseif($day == 0 && $hour > 0)
	   {
		   $day = 1;
		  }
		elseif($hour > 0)
		{
			$day+=1;
		}
		$grand = abs($booking->total_price - $booking->advance);
		$css = <<<css
.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;  
  height: 17.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background: url(dimension.png);
}

.project {
  float: left;
}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 20px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 85%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
css;
if(!$booking->advance == 0){
$adv = <<<eot
<tr style="background: #F5F5F5;">
            <td style="color:red;" colspan="4">ADVANCE PAID</td>
            <td style="color:red; text-align:left;" class="total">{$company->currency} {$booking->advance}</td>
          </tr>
eot;
}
else{
$adv = null;
}
if($booking->total_price < $booking->advance){
$return = <<<eot
<tr>
            <td style="color:red;" colspan="4">RETURN (-)</td>
            <td style="color:red; text-align:left;" class="total">{$company->currency} {$grand}</td>
          </tr>
eot;
}
else{
$return = null;
}



if($booking->total_price < $booking->advance){
		$ttl = 0;
}
elseif(!$booking->advance == 0){
	$ttl = $booking->total_price - $booking->advance;
}
else{
	$ttl = $booking->total_price;
}


$html = <<<eot
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{$customer->customer_name} - Invoice</title>
    <style>
	@page { sheet-size: 200mm 195mm; }
	{$css}
	</style>
  </head>
    <body>
    <header class="clearfix">
      <h1>INVOICE</h1>
	  <table>
	  <tbody>
          <tr>
            <td style="text-align: left; background: none;">
        <div>{$company->name}</div>
        <div>{$company->address}</div>
        <div>{$company->phone}</div>
        <div><a href="mailto:{$company->email}">{$company->email}</a></div>
      </td>
            <td style="text-align: right; background: none;">
        <div>Name : {$customer->customer_name}</div>
        <div>Address : {$customer->address}</div>
        <div>Phone : {$customer->contact_no}</div>
        <div>Booking date : {$booking_date}</div>
        <div>Checked-in : {$check_in}</div>
		<div>Checked-out : {$check_out}</div>
		</td>
          </tr>
	  </tbody>
      </table>
	  
    </header>
	
	
	
    <main>
      <table>
        <thead>
          <tr>
            <th style="text-align:left;">ROOM NO</th>
            <th style="text-align:left;">ROOMTYPE</th>
            <th style="text-align:left;">STAYING DAY</th>
            <th style="text-align:left;">PRICE x STAYING DAY</th>
            <th style="text-align:left;">TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="text-align:left;">{$room->room_no}</td>
            <td style="text-align:left;">{$roomtype->room_type}</td>
            <td style="text-align:left;">{$day}</td>
            <td style="text-align:left;">{$roomtype->price}x{$day}</td>
            <td style="text-align:left;">{$company->currency} {$booking->total_price}</td>
          </tr>
          {$adv}
		  {$return}
          <tr style="background: #F5F5F5;">
            <td colspan="4" class="grand total">TOTAL PAYABLE AMOUNT</td>
			<td style="text-align:left;" class="grand total">{$company->currency} {$ttl}</td>
          </tr>
		  
        </tbody>
      </table>
    </main>
    <footer>
	{$company->footer}
    </footer>
  </body>
</html>
eot;

	require_once __DIR__ . '/vendor/autoload.php';
	$mpdf = new \Mpdf\Mpdf();
	$mpdf->SetWatermarkText($company->watermark);
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;

$mpdf->WriteHTML($html);
if($booking->payment_status == 1){
$mpdf->Output("{$customer->customer_name} - {$customer->id}.pdf", \Mpdf\Output\Destination::INLINE);
}
else{
	return redirect()->route('home')->with('alert', 'Payment not clear.');
}

	}
	}