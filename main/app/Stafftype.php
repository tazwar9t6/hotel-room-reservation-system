<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stafftype extends Model
{
    protected $table = 'staff_type';
    protected $fillable = array('staff_type');
}
