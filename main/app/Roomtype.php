<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomtype extends Model
{
    protected $table = 'room_type';
    protected $fillable = array( 'room_type','price', 'max_person');
}
