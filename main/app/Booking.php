<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'booking';
    protected $fillable = array('customer_id','room_id','booking_date','check_in','check_out','total_price','advance','remaining_price','payment_status', 'request');
	
	public function Customer()
	{
		return $this->belongsTo('App\Customer', 'customer_id','id');
	}
	
	public function Room()
	{
		return $this->belongsTo('App\Room', 'room_id','id');
	}
	
}
