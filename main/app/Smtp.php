<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Smtp extends Model
{
    protected $table = 'smtp';
    protected $fillable = array('smtp','smtp_host', 'smtp_user', 'smtp_pass', 'smtp_port', 'smtp_secure');
}
