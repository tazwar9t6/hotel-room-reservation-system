<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emphistory extends Model
{
    protected $table = 'emp_history';
    protected $fillable = array('emp_id','shift_id','from_date','to_date');
}
