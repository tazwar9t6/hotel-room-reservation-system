<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'room';
    protected $fillable = array( 'room_type_id','room_no', 'status', 'check_in_status', 'check_out_status','deleteStatus');
}
