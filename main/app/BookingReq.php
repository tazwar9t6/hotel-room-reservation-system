<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingReq extends Model
{
    protected $table = 'request_booking';
    protected $fillable = array('customer_id','room_type','booking_date','check_in','check_out','status');
	
	public function Customer()
	{
		return $this->belongsTo('App\Customer', 'customer_id','id');
	}
	
	public function RoomType()
	{
		return $this->belongsTo('App\Roomtype', 'room_type','id');
	}
	
}
