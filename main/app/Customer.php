<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';
    protected $fillable = array( 'customer_name','contact_no', 'email', 'id_card_type_id', 'id_card_no','address');
	
	public function Idcardtype()
	{
		return $this->belongsTo('App\Idcardtype', 'id_card_type_id','id');
	}
	
}
