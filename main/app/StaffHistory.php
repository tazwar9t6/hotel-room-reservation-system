<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffHistory extends Model
{
    protected $table = 'emp_history';
    protected $fillable = array( 'emp_id','shift_id','to_date');
}
