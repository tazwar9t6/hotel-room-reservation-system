@extends('admin.layout.head')
@section('content')
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">All Staffs</li>
			</ol>

			<h1 class="page-header">All Staffs <button type="button" data-toggle="modal" data-target="#addemp" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New Staff</button> <button type="button" data-toggle="modal" data-target="#addPositioin" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New Staff Position</button> <a type="button" data-toggle="modal" data-target="#addShift" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New Shift</a></h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All Staffs</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Staff name</th>
                                        <th>Position</th>
                                        <th>Shift</th>
                                        <th>Joining date</th>
                                        <th>Salary</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								@foreach($staffs as $staff)
								@php
								$position = \App\Stafftype::where('id',$staff->staff_type_id)->first();
								$shift = \App\Shift::where('id',$staff->shift_id)->first();
								$join = date('M j, Y', strtotime($staff->joining_date));
								@endphp
                                        <td><b style="font-size:13px;">{{$staff->emp_name}}</b></td>
                                        <td><b style="font-size:13px;">{{$position->staff_type}}</b></td>
                                        <td><b style="font-size:13px;">{{$shift->shift}} - {{$shift->shift_timing}}</b></td>
                                        <td><b style="font-size:13px;">{{$join}}</b></td>
                                        <td><b style="font-size:13px;">{{$staff->salary}}</b></td>
										<td>
											<a data-toggle="modal" data-target="#changeShift{{$staff->id}}" class="btn btn-info btn-icon btn-circle btn-lg"><i class="fa fa-arrow-circle-o-right fa-rotate-270"></i></a>
											
											<a href="{{route('history', [$staff->id])}}" class="btn btn-info btn-icon btn-circle btn-lg"><i class="fa fa-eye"></i></a>
										
										<a data-toggle="modal" data-target="#editemp{{$staff->id}}" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-pencil"></i></a>
										<a onclick="confirm_click{{$staff->id}}();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                                <script type="text/javascript">
										function confirm_click{{$staff->id}}()
										{
											if (confirm("Are you sure?")) {
												event.preventDefault();
												document.getElementById('delete-form{{$staff->id}}').submit();
												} else {
													return false;
													}
											}
											</script>
										<form id="delete-form{{$staff->id}}" action="{{ route('removeStaff') }}" method="post" style="display: none;">
										{{ csrf_field() }}
										<input type="hidden" name="id" value="{{$staff->id}}">
										</form>
										</td>
										
										
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
    <div id="addemp" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Staff</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('addemp') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
                                <div class="form-group">
                                    <label>Position</label>
                                    <select class="form-control" name="position" required>
                                        <option selected disabled>Select Position</option>
										@php
										$staff_types = \App\Stafftype::all();
										@endphp
										@foreach($staff_types as $type)
										<option value="{{$type->id}}">{{$type->staff_type}}</option>
										@endforeach
										</select>
                                </div>
								<div class="form-group">
                                    <label>Shift</label>
                                    <select class="form-control" name="shift" required>
                                        <option selected disabled>Select Shift</option>
										@php
										$shifts = \App\Shift::all();
										@endphp
										@foreach($shifts as $shift)
										<option value="{{$shift->id}}">{{$shift->shift}} - {{$shift->shift_timing}}</option>
										@endforeach
										</select>
                                </div>

                                <div class="form-group">
                                    <label>Staff name</label>
                                    <input class="form-control" type="text" placeholder="Staff Name" name="name" required>
                                </div>
								<div class="form-group">
                                    <label>Select Id card</label>
                                    <select class="form-control" name="idcard" required>
                                        <option selected disabled>Select Id card</option>
										@php
										$idcards = \App\Idcardtype::all();
										@endphp
										@foreach($idcards as $idcard)
										<option value="{{$idcard->id}}">{{$idcard->id_card_type}}</option>
										@endforeach
										</select>
                                </div>
								<div class="form-group">
                                    <label>Id card no</label>
                                    <input class="form-control" type="text" placeholder="Id card no" name="id_card_no" required>
                                </div>
								<div class="form-group">
                                    <label>Contact number</label>
                                    <input class="form-control" type="number" placeholder="Contact number" name="number" required>
                                </div>
								<div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" type="text" placeholder="Address" name="address" required>
                                </div>
								<div class="form-group">
                                    <label>Salary</label>
                                    <input class="form-control" type="text" placeholder="Staff salary" name="salary" required>
                                </div>
								
								
								
                                <button class="btn btn-success pull-right">Add Staff</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	
	    <div id="addPositioin" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Staff Position</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('addPositioin') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
                                <div class="form-group">
                                    <label>Position</label>
                                    <input class="form-control" placeholder="Staff Position" name="name" required>
                                </div>
                                <button class="btn btn-success pull-right">Add Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<div id="addShift" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Staff Position</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('addShift') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
                                <div class="form-group">
                                    <label>Shift</label>
                                    <input class="form-control" placeholder="Shift" name="shift" required>
                                </div>
								<div class="form-group">
                                    <label>Shift Timing</label>
                                    <input class="form-control" placeholder="Shift Timing" name="timing" required>
                                </div>
                                <button class="btn btn-success pull-right">Add Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	
	@foreach($staffs as $staff)
        <div id="changeShift{{$staff->id}}" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Change Shift</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <form data-toggle="validator" role="form" action="{{ route('changeShift') }}" method="post">
										{{ csrf_field() }}
										<input type="hidden" name="staff_id" value="{{$staff->id}}">
                                            <div class="row">
                                            <div class="form-group col-lg-12">
                                                <label>Shift</label>
                                                <select class="form-control" name="shift_id" required>
                                                    <option selected disabled>Select Shift</option>
                                                   @php
												   $shifts = \App\Shift::all();
												   @endphp
												   @foreach($shifts as $shift)
												   <option value="{{$shift->id}}" @if ($shift->id == $staff->shift_id) selected @endif >{{$shift->shift}} - {{$shift->shift_timing}}</option>
												   @endforeach
                                                </select>
                                            </div>
                                            </div>
                                            <button class="btn btn-success pull-right">Change Shift</button>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>

            </div>
        </div>
	@endforeach
	
	@foreach($staffs as $staff)
        <div id="editemp{{$staff->id}}" class="modal fade" role="dialog">
                 <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Staff</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('editStaff') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{$staff->id}}">
                                <div class="form-group">
                                    <label>Position</label>
                                    <select class="form-control" name="position" required>
                                        <option selected disabled>Select Position</option>
										@php
										$staff_types = \App\Stafftype::all();
										@endphp
										@foreach($staff_types as $type)
										<option value="{{$type->id}}"@if ($type->id == $staff->staff_type_id) selected @endif>{{$type->staff_type}}</option>
										@endforeach
										</select>
                                </div>
								<div class="form-group">
                                    <label>Shift</label>
                                    <select class="form-control" name="shift" required>
                                        <option selected disabled>Select Shift</option>
										@php
										$shifts = \App\Shift::all();
										@endphp
										@foreach($shifts as $shift)
										<option value="{{$shift->id}}"@if ($shift->id == $staff->shift_id) selected @endif>{{$shift->shift}} - {{$shift->shift_timing}}</option>
										@endforeach
										</select>
                                </div>

                                <div class="form-group">
                                    <label>Staff name</label>
                                    <input class="form-control" type="text" placeholder="Staff Name" value="{{$staff->emp_name}}" name="name" required>
                                </div>
								<div class="form-group">
                                    <label>Select Id card</label>
                                    <select class="form-control" name="idcard" required>
                                        <option selected disabled>Select Id card</option>
										@php
										$idcards = \App\Idcardtype::all();
										@endphp
										@foreach($idcards as $idcard)
										<option value="{{$idcard->id}}"@if ($idcard->id == $staff->id_card_type) selected @endif>{{$idcard->id_card_type}}</option>
										@endforeach
										</select>
                                </div>
								<div class="form-group">
                                    <label>Id card no</label>
                                    <input class="form-control" type="text" value="{{$staff->id_card_no}}" placeholder="Id card no" name="id_card_no" required>
                                </div>
								<div class="form-group">
                                    <label>Contact number</label>
                                    <input class="form-control" type="number" value="{{$staff->contact_no}}" placeholder="Contact number" name="number" required>
                                </div>
								<div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" type="text" value="{{$staff->address}}" placeholder="Address" name="address" required>
                                </div>
								<div class="form-group">
                                    <label>Salary</label>
                                    <input class="form-control" type="text" value="{{$staff->salary}}" placeholder="Staff salary" name="salary" required>
                                </div>
								
								
								
                                <button class="btn btn-success pull-right">Edit Staff</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@endforeach
	
@endsection
@section('extracss')
<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"/>
@endsection
@section('extrajs')
<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-default.demo.min.js')}}"></script>
<script src="{{asset('assets/js/ajax.js')}}"></script>
@endsection
@section('extrainit')
TableManageDefault.init();
@endsection