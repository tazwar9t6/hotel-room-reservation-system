@extends('admin.layout.head')
@section('content')
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Invoice</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Invoice Data</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			      <div class="col-md-12">
                        <div class="panel-body">
						<div class="embed-responsive embed-responsive-16by9">
						<object class="embed-responsive-item" data="{{route('showinvoice', $invoice)}}" type="application/pdf" internalinstanceid="9" title="">
						</object>
						</div>
                        </div>
                </div>
                <!-- end col-6 -->
                <!-- begin col-6 -->
            </div>
			
@endsection