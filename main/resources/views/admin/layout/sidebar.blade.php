<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<div class="image">
							<a><img src="{{ asset('assets/img/user-13.jpg') }}" alt="" /></a>
						</div>
						<div class="info">
							{{$admin->name}}
							<small>@if($admin->type == "admin")Administrator @else {{ ucfirst($admin->type)}} @endif</small>
						</div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Navigation</li>
					<li class="@if(request()->path() == 'admin/home') active @endif"><a href="{{route('home')}}"><i class="fa fa-laptop"></i> <span>Dashboard</span></a></li>
					<li class="has-sub @if(request()->path() == 'admin/allroom') active 
							@elseif(request()->path() == 'admin/roomtypes') active
							@elseif(request()->path() == 'admin/reqreservation') active
							@elseif(request()->path() == 'admin/reservation') active
							@elseif(request()->is('admin/reservation/*/*')) active
							@endif">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-bed"></i>
						    <span>Room Management</span> 
						</a>
                    <ul class="sub-menu">					
					<li class="@if(request()->path() == 'admin/allroom') active @endif"><a href="{{route('allroom')}}">All Rooms</a></li>
					<li class="@if(request()->path() == 'admin/reservation') active @endif @if(request()->is('admin/reservation/*/*')) active @endif"><a href="{{ route('reservation') }}">Reservation</a></li>
					<li class="@if(request()->path() == 'admin/reqreservation') active @endif @if(request()->is('admin/reqreservation/*/*')) active @endif"><a href="{{ route('reqreservation') }}">Reservation Request</a></li>
					<li class="@if(request()->path() == 'admin/roomtypes') active @endif"><a href="{{route('roomtypes')}}">Room Types</a></li>
					</ul>
					</li>
					@if($admin->type == "admin")
					<li class="has-sub @if(request()->path() == 'admin/allstaff') active 
							@elseif(request()->path() == 'admin/staffPosition') active
							@elseif(request()->path() == 'admin/allShift') active
							@elseif(request()->is('admin/allstaff/history/*')) active
							@endif">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-users"></i>
						    <span>Staff Management</span> 
						</a>
                    <ul class="sub-menu">					
					<li class="@if(request()->path() == 'admin/allstaff') active @endif @if(request()->is('admin/allstaff/history/*')) active @endif"><a href="{{route('allstaff')}}">All Staff</a></li>
					<li class="@if(request()->path() == 'admin/staffPosition') active @endif"><a href="{{route('staffPosition')}}">Staff Positions</a></li>
					<li class="@if(request()->path() == 'admin/allShift') active @endif"><a href="{{route('allShift')}}">Manage Shift</a></li>
					
					</ul>
					</li>
					@endif
					<li class="@if(request()->path() == 'admin/allusers') active @endif"><a href="{{route('allusers')}}"><i class="fa fa-smile-o"></i> <span>All Booking history</span></a></li>
					@if($admin->type == "admin")
					<li class="@if(request()->path() == 'admin/Idcards') active @endif"><a href="{{route('Idcards')}}"><i class="fa fa-credit-card"></i> <span>Manage ID Card Types</span></a></li>
				    @endif
					<li class="@if(request()->path() == 'admin/sysusers') active @endif"><a href="{{route('sysusers')}}"><i class="fa fa-user"></i> <span>System Users</span></a></li>
					<li class="@if(request()->path() == 'admin/invoice') active @endif"><a href="{{route('invoice')}}"><i class="fa fa-print"></i> <span>Invoice Settings</span></a></li>
			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>