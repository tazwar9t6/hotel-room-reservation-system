@if (session('success'))
<div class="alert alert-success fade in m-b-15">
<strong>Success!</strong> {{ session('success') }} <span class="close" data-dismiss="alert">×</span>
</div>
@endif
@if (session('alert'))
<div class="alert alert-warning fade in m-b-15">
<strong>Alert!</strong> {{ session('alert') }} <span class="close" data-dismiss="alert">×</span>
</div>
@endif
@if ($errors->any())
@foreach ($errors->all() as $error)
<div class="alert alert-warning fade in m-b-15">
<strong>Sorry!</strong> {{ $error }} <span class="close" data-dismiss="alert">×</span>
</div>
@endforeach
@endif