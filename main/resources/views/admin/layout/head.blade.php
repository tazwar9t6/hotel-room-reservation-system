<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Room Management System Dashboard</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style-responsive.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/theme/default.css') }}" rel="stylesheet" id="theme" />
	<link href="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
	@yield('extracss')
	<script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
	<link rel="icon" href="{{asset('assets/img/icon.png') }}" type="image/x-icon" />
</head>


<body>
	<div id="page-loader" class="fade in"><span class="spinner"></span></div></div>

<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">

<div id="header" class="header navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand"><span class="navbar-logo"></span> <b style="font-size:15px;">Room Management</b> </a>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown navbar-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{ asset('assets/img/user-13.jpg') }}" alt="" /> 
							<span class="hidden-xs">{{$admin->name}}</span> <b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<li><a href="{{route('sysusers')}}">Edit Profile</a></li>
							<li class="divider"></li>
							<li><a href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">Log Out</a></li>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
						</ul>
					</li>
				</ul>
			</div>
		</div>
@include('admin.layout.sidebar')
<div id="content" class="content">
@include('admin.layout.alert')
@yield('content')
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

	<script src="{{asset('assets/plugins/jquery/jquery-1.9.1.min.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js') }}"></script>
	<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<!--[if lt IE 9]>
		<script src="{{asset('assets/crossbrowserjs/html5shiv.js') }}"></script>
		<script src="{{asset('assets/crossbrowserjs/respond.min.js') }}"></script>
		<script src="{{asset('assets/crossbrowserjs/excanvas.min.js') }}"></script>
	<![endif]-->
	<script src="{{asset('assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery-cookie/jquery.cookie.js') }}"></script>
	@yield('extrajs')
	<script src="{{asset('assets/plugins/gritter/js/jquery.gritter.js') }}"></script>
	<script src="{{asset('assets/plugins/flot/jquery.flot.min.js') }}"></script>
	<script src="{{asset('assets/plugins/flot/jquery.flot.time.min.js') }}"></script>
	<script src="{{asset('assets/plugins/flot/jquery.flot.resize.min.js') }}"></script>
	<script src="{{asset('assets/plugins/flot/jquery.flot.pie.min.js') }}"></script>
	<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
	<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
	<script src="{{asset('assets/js/dashboard.min.js') }}"></script>
	<script src="{{asset('assets/js/apps.min.js') }}"></script>	
	<script>
		$(document).ready(function() {
			App.init();
			@yield('extrainit')
		});
	</script>
</body>
</html>
