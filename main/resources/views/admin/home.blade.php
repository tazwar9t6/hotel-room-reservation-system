@extends('admin.layout.head')
@section('content')
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-green">
						<div class="stats-icon"><i class="fa fa-bed"></i></div>
						<div class="stats-info">
							<h4>TOTAL ROOM</h4>
							<p>{{$rooms}}</p>	
						</div>
						<div class="stats-link">
							<a href="{{route('allroom')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-bed"></i></div>
						<div class="stats-info">
							<h4>AVAILABLE ROOM</h4>
							<p>{{$activeroom}}</p>	
						</div>
						<div class="stats-link">
							<a href="{{route('allroom')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-purple">
						<div class="stats-icon"><i class="fa fa-users"></i></div>
						<div class="stats-info">
							<h4>RESERVATION REQUEST</h4>
							<p>{{$request}}</p>	
						</div>
						<div class="stats-link">
							<a href="{{route('reqreservation')}}">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-3 col-sm-6">
					<div class="widget widget-stats bg-red">
						<div class="stats-icon"><i class="fa fa-clock-o"></i></div>
						<div class="stats-info">
							<h4>TOTAL STAFF</h4>
							<p>{{$staff}}</p>	
						</div>
						
						<div class="stats-link">
							<a @if($admin->type == "admin") href="{{route('allstaff')}}" @endif>View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
						</div>
						
					</div>
				</div>
				<!-- end col-3 -->
			</div>
			<!-- end row -->
			<!-- begin row -->
			<div class="row">
				<!-- begin col-8 -->
				<div class="col-sm-8">
					<div class="panel panel-inverse" data-sortable-id="table-basic-1">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Latest Room Reservation</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Room No</th>
                                        <th>Type</th>
										<th>Checked In</th>
                                    </tr>
                                </thead>
                                <tbody>
								@foreach($latests as $i=>$latest)
								@php
								$room = \App\Room::where('id',$latest->room_id)->first();
								$room_type = \App\Roomtype::where('id',$room->room_type_id)->first();
								@endphp
                                    <tr>
                                        <td><b style="font-size:12px;">{{$i+1}}</b></td>
                                        <td><b style="font-size:12px;">{{$room->room_no}}</b></td>
                                        <td><b style="font-size:12px;">{{$room_type->room_type}}</b></td>
										@php
										$date = date('M j, Y - g:i A', strtotime($latest->check_in))
										@endphp
										<td><b style="font-size:12px;">{{$date}}</b></td>
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-inverse" data-sortable-id="index-2">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Latest Guest</h4>
						</div>
						<div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
								@foreach($customers as $x=>$customer)
                                    <tr>
                                        <td><b style="font-size:12px;">{{$x+1}}</b></td>
                                        <td><b style="font-size:12px;">{{$customer->customer_name}}</b></td>
                                        <td><b style="font-size:12px;">{{$customer->contact_no}}</b></td>
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
					</div>
			</div>
			</div>
			<!-- end row -->
@endsection