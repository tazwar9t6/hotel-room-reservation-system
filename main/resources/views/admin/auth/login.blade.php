<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Hotel Management System</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="{{asset('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/css/animate.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/css/style.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/css/style-responsive.min.css') }}" rel="stylesheet" />
	<link href="{{asset('assets/css/theme/default.css') }}" rel="stylesheet" id="theme" />
	<link href="{{asset('assets/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />	
	<script src="{{asset('assets/plugins/pace/pace.min.js') }}"></script>
	<!-- ================== END BASE JS ================== -->
	<link rel="icon" href="{{asset('assets/img/icon.png') }}" type="image/x-icon" />
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>
	<!-- end #page-loader -->

	<div id="page-container" class="fade">
	    <!-- begin login -->
        <div class="login bg-black animated fadeInDown">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand" style="font-weight: 400;">
                    <span class="logo"></span> Hotel Management
                    <small>Login to continue</small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">
                <form method="POST" class="margin-bottom-0">
                    <div class="form-group m-b-20">
                        <input type="text" id="user" class="form-control input-lg inverse-mode no-border" placeholder="Username" required />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="password" id="pass" class="form-control input-lg inverse-mode no-border" placeholder="Password" required />
                    </div>
                    <div class="checkbox m-b-20">
                        <label>
                            <input id="remember" type="checkbox" /> Remember Me
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button type="submit" id="login" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading" class="btn btn-success btn-block btn-lg">Sign me in</button>
                    </div>
					<div class="m-t-20">
                        Forget Password? Click <a href="#">here</a> to reset.
                    </div>
                </form>
            </div>
        </div>
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{asset('assets/plugins/jquery/jquery-1.9.1.min.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js') }}"></script>
	<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<!--[if lt IE 9]>
		<script src="{{asset('assets/crossbrowserjs/html5shiv.js') }}"></script>
		<script src="{{asset('assets/crossbrowserjs/respond.min.js') }}"></script>
		<script src="{{asset('assets/crossbrowserjs/excanvas.min.js') }}"></script>
	<![endif]-->
	<script src="{{asset('assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{asset('assets/plugins/jquery-cookie/jquery.cookie.js') }}"></script>
	<script src="assets/plugins/gritter/js/jquery.gritter.js"></script>
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{asset('assets/js/apps.min.js') }}"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
   <script>

  $(document).ready(function(){
    $(document).on('click','#login',function(e){
      e.preventDefault();
	  var $this = $(this);
	  $this.button('loading');
      var username = $('#user').val();
      var password = $('#pass').val();
	  
	  if ($('#remember').is(':checked')) {
		  var remember = 1;
      }
	  else{
		  var remember = null;
	  }
      $.ajax({
       type:'POST',
       url:'{{ route('admin.login') }}',
       data:{
		   _token: '{{ csrf_token() }}',
		   username:username,
		   password:password,
		   remember
		   },
       success:function(data){
		$this.button('reset');
		window.location.href="{{asset('admin/home') }}";						
      },
      error:function (error) {
        var message = JSON.parse(error.responseText);
			$.gritter.add({
            title: message.message,
            text: message.errors.username,
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		$this.button('reset');
      }
    });
    });
  });  
   </script>
</body>
</html>
