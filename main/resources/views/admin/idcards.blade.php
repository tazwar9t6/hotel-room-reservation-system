@extends('admin.layout.head')
@section('content')
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">ID Card Types</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">ID Card Types <button type="button" data-toggle="modal" data-target="#addStafftype" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New ID Card Types</button> </h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All ID Card Types</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" style="font-size:13px;font-weight: bold;"  class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Card Types</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								@foreach($cards as $no=>$type)
                                        <td>{{$no+1}}</td>
                                        <td>{{$type->id_card_type}}</td>
										<td>
										<a data-toggle="modal" data-target="#edit{{$type->id}}" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-pencil"></i></a>
										<a onclick="confirm_click();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                               <script type="text/javascript">
										function confirm_click()
										{
											if (confirm("Are you sure?")) {
												event.preventDefault();
												document.getElementById('delete-form{{$type->id}}').submit();
												} else {
													return false;
													}
											}
										</script>
										<form id="delete-form{{$type->id}}" action="{{ route('removeIdcard') }}" method="post" style="display: none;">
										{{ csrf_field() }}
										<input type="hidden" name="id" value="{{$type->id}}">
										</form>
										</td>
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
				    <!-- Add Room Type -->
    <div id="addStafftype" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New ID Card Type</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('addIdcard') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
                                <div class="form-group">
                                    <label>ID Card Type</label>
                                    <input class="form-control" placeholder="ID Card Type" name="name" required>
                                </div>
                                <button class="btn btn-success pull-right">Add Position</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@foreach($cards as $type1)
	<div id="edit{{$type1->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit ID Card Type</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('editIdcard') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{$type1->id}}">
                                <div class="form-group">
                                    <label>ID Card Type</label>
                                    <input class="form-control" placeholder="ID Card Type" value="{{$type1->id_card_type}}" name="name" required>
                                </div>
                                <button class="btn btn-success pull-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@endforeach	
@endsection

@section('extracss')
<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"/>
@endsection
@section('extrajs')
<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-default.demo.min.js')}}"></script>
@endsection
@section('extrainit')
TableManageDefault.init();
@endsection