@extends('admin.layout.head')
@section('content')
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Room Management</a></li>
				<li class="active">Reservation</li>
			</ol>
<h1 class="page-header">Reservation</h1>

    <div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Room Information</h4>
                        </div>
					<div class="panel-body">
            <form role="form" id="booking" autocomplete="off">
                <div class="col-lg-12">
								<div class="row">
							<div class="col-6 col-md-4">
							<div class="widget widget-stats bg-black">
							<div class="stats-info">
							<h4 style="font-weight: bold; font-size:15px;">TOTAL DAYS : 
							<span id="staying_day">0</span> Days</h4>
							</div>
							</div>
							</div>
							<div class="col-6 col-md-4">
							<div class="widget widget-stats bg-black">
							<div class="stats-info">
							<h4 style="font-weight: bold; font-size:15px;">ROOM PRICE : 
							<span id="price">0</span> /-</h4>
							</div>
							</div>
							</div>
							<div class="col-6 col-md-4">
							<div class="widget widget-stats bg-black">
							<div class="stats-info">
							<h4 style="font-weight: bold; font-size:15px;">TOTAL AMOUNT : 
							<span id="total_price">0</span> /-</h4>
							</div>
							</div>
							</div>
							</div>
							
                                <div class="form-group col-lg-6">
                                    <label>Room Type</label>
									<select class="form-control" id="room_type" onchange="get_rooms(this.value);" required>
                                        <option selected disabled>Select Room Type</option>
                                        @foreach($room_types as $type)
										<option value="{{$type->id}}">{{$type->room_type}}</option>
										@endforeach
										</select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Room No</label>
                                    <select class="form-control" id="room_no" onchange="get_price(this.value);" required>
                                        <option selected disabled>Select Room</option>
										</select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check In Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_in_date"  required>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check Out Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_out_date" required>
                                </div>
                            </div>
                        </div>
						</div>
						</div>
                        </div>
						
						
						
						<div class="row">
			    <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Customer Detail</h4>
                        </div>
                        <div class="panel-body">
						<div class="col-lg-12">
                            <div class="form-group col-lg-6">
                                   <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="First Name" id="first_name">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Last Name</label>
                                     <input class="form-control" type="text" placeholder="Last Name" id="last_name">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Contact No</label>
                                    <input type="number" class="form-control" placeholder="Contact No" id="contact_no">
                                </div>

								<div class="form-group col-lg-6">
                                     <label>Email Address</label>
                                    <input type="text" class="form-control" placeholder="Email Address" id="email">
                                </div>

                                <div class="form-group col-lg-6">
                                   <label>ID Card Type</label>
                                    <select class="form-control" id="id_card_type" required>
                                    <option selected disabled>Select ID Card Type</option>
                                        @foreach($idcards as $card)
										<option value="{{$card->id}}">{{$card->id_card_type}}</option>
										@endforeach
										</select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>ID Card No</label>
                                     <input type="text" class="form-control" placeholder="ID Card No" id="id_card_no">
                                </div>
								
								
                            <div class="form-group col-lg-12">
                                <label>Address</label>
                                <textarea class="form-control" rows="3" id="address"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
							</div>
                        </div>
                    </div>
				</div>
				</div>
				<div class="form-group text-center">
                <button type="submit" id="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Please wait" class="btn btn-primary btn-lg m-r-6">Submit</button>
				</div>
            </form>
<div id="bookingConfirm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Room Booking</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div style="font-size:15px;" class="alert bg-success alert-dismissable" role="alert"><em class="fa fa-lg fa-check-circle">&nbsp;</em><b>Room Successfully Booked</b></div>
                        <table style="font-size:13px;font-weight: bold;" class="table table-striped table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Customer Name</td>
                                <td id="getCustomerName"></td>
                            </tr>
                            <tr>
                                <td>Room Type</td>
                                <td id="getRoomType"></td>
                            </tr>
                            <tr>
                                <td>Room No</td>
                                <td id="getRoomNo"></td>
                            </tr>
                            <tr>
                                <td>Check In</td>
                                <td id="getCheckIn"></td>
                            </tr>
                            <tr>
                                <td>Check Out</td>
                                <td id="getCheckOut"></td>
                            </tr>
                            <tr>
                                <td>Total Amount</td>
                                <td id="getTotalPrice"></td>
                            </tr>
                            <tr>
                                <td>Payment Status</td>
                                <td id="getPaymentStaus"></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" data-dismiss="modal">Okay</a>
            </div>
        </div>
    </div>
</div>
<script>
function get_rooms(val) {
    $.ajax({
        type: 'post',
        url: '{{ route('getrooms') }}',
        data: {
			_token : '{{ csrf_token() }}',
            id: val
        },
        success: function (response) {
            $('#room_no').html(response);
        }
    });
}
function get_price(val) {
    $.ajax({
        type: 'post',
        url: '{{ route('getprice') }}',
        data: {
			_token : '{{ csrf_token() }}',
            id: val
        },
        success: function (response) {
            $('#price').html(response);
            var days = document.getElementById('staying_day').innerHTML;
            $('#total_price').html(response*days);
        }
    });
}
</script>
<script>
    var booking = "{{ route('booking') }}";
	var token = "{{ csrf_token() }}";
</script>

@endsection

@section('extracss')
<link href="{{asset('assets/css/foundation-datepicker.css')}}" rel="stylesheet" />
@endsection
@section('extrajs')
<script src="{{asset('assets/js/foundation-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/booking.js')}}"></script>
@endsection