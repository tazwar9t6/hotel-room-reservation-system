@extends('admin.layout.head')
@section('content')
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Room Management</a></li>
				<li class="active">Reservation requests</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Reservation requests</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All reservation requests</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" style="font-size:13px;font-weight: bold;" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Customer name</th>
                                        <th>Room type</th>
                                        <th>Booking date</th>
										<th>Check in</th>
										<th>Check out</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								@foreach($requests as $request)
                                        <td>{{$request->Customer->customer_name}}</td>
                                        <td>{{$request->RoomType->room_type}}</td>
                                        <td>{{date('M j, Y - g:i A', strtotime($request->booking_date))}}</td>
                                        <td>{{date('M j, Y', strtotime($request->check_in))}}</td>
                                        <td>{{date('M j, Y', strtotime($request->check_out))}}</td>
										<td><a href="{{route('req.book',[$request->id,$request->RoomType->id, $request->Customer->id])}}" class="btn btn-info btn-icon btn-circle btn-lg"><i class="fa fa-arrow-right"></i></a>
										<a data-toggle="modal" data-target="#cutomerDetail" data-id="{{$request->Customer->id}}" id="usreDetails" class="btn btn-info btn-icon btn-circle btn-lg"><i class="fa fa-eye"></i></a>
										<a onclick="confirm_click();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                               <script type="text/javascript">
										function confirm_click()
										{
											if (confirm("Remove this request ?")) {
												event.preventDefault();
												document.getElementById('delete-form{{$request->id}}').submit();
												} else {
													return false;
													}
											}
										</script>
										<form id="delete-form{{$request->id}}" action="{{ route('removebookingreq') }}" method="post" style="display: none;">
										{{ csrf_field() }}
										<input type="hidden" name="id" value="{{$request->id}}">
										</form>
										</td>
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
<div id="cutomerDetail" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Customer Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table style="font-size:13px;font-weight: bold;" class="table table-responsive table-bordered">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td id="customer_name"></td>
                                </tr>
                                <tr>
                                    <td>Contact Number</td>
                                    <td id="customer_contact_no"></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td id="customer_email"></td>
                                </tr>
                                <tr>
                                    <td>ID Card Type</td>
                                    <td id="customer_id_card_type"></td>
                                </tr>
                                <tr>
                                    <td>ID Card Number</td>
                                    <td id="customer_id_card_number"></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td id="customer_address"></td>
                                </tr>
								<tr>
                                    <td>Booking date</td>
                                    <td id="booking_date"></td>
                                </tr>
                                <tr>
                                    <td>Checked In</td>
                                    <td id="check_id"></td>
                                </tr>
                                <tr>
                                    <td>Checked Out</td>
                                    <td id="check_out"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
				<div class="modal-footer">
											<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
										</div>
            </div>
        </div>
    </div>
<script>
    var usreDetails = "{{ route('cusDetails') }}";
	var token = "{{ csrf_token() }}";
</script>	
@endsection

@section('extracss')
<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"/>
@endsection
@section('extrajs')
<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-default.demo.min.js')}}"></script>
<script src="{{asset('assets/js/ajax.js')}}"></script>
@endsection
@section('extrainit')
TableManageDefault.init();
@endsection