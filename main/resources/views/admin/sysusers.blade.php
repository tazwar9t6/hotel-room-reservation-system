@extends('admin.layout.head')
@section('content')
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">System User Management</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">System Users @if($admin->type == "admin")<button type="button" data-toggle="modal" data-target="#addsysuser" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New System User</button>@endif </h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All Users</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" style="font-size:13px;font-weight: bold;"  class="table table-striped table-bordered">
                                <thead>
                                    <tr>
										<th>Profile name</th>
                                        <th>Username</th>
										<th>Email</th>
										<th>Role</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								@foreach($admins as $user)
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->username}}</td>
										<td>{{$user->email}}</td>
										<td>@if($user->type == "admin")Administrator @else {{ ucfirst($user->type)}} @endif</td>
										<td>
										<a data-toggle="modal" data-target="#passwd{{$user->id}}" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-key"></i></a>
										<a data-toggle="modal" data-target="#edit{{$user->id}}" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-pencil"></i></a>
										
										@if($user->id == $admin->id)
										@else
										<a onclick="confirm_click();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                               <script type="text/javascript">
										function confirm_click()
										{
											if (confirm("Are you sure?")) {
												event.preventDefault();
												document.getElementById('delete-form{{$user->id}}').submit();
												} else {
													return false;
													}
											}
										</script>
										<form id="delete-form{{$user->id}}" action="{{ route('RemoveSysUser') }}" method="post" style="display: none;">
										{{ csrf_field() }}
										<input type="hidden" name="id" value="{{$user->id}}">
										</form>
										@endif
										</td>
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
				    <!-- Add Room Type -->
	@if($admin->type == "admin")
    <div id="addsysuser" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New System User</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('addsysuser') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
                                <div class="form-group">
                                    <label>Profile Name</label>
                                    <input class="form-control" placeholder="Profile Name" name="name" required>
                                </div>
								<div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" placeholder="Username" name="user" required>
                                </div>
								<div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" placeholder="Email" name="email" required>
                                </div>
								<div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" placeholder="Password" name="pass" required>
                                </div>
								<div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control" name="role" required>
									<option selected disabled>Select User Role</option>
									<option value="1">Administrator</option>
									<option value="0">Manager</option>
									</select>
                                </div>
								
                                <button class="btn btn-success pull-right">Add User</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@endif
	@foreach($admins as $user)
	<div id="passwd{{$user->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change System User Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('syschngpass') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{$user->id}}">
                                @if($user->id == $admin->id)
								<div class="form-group">
                                    <label>Old Password</label>
                                    <input class="form-control" placeholder="Old password" name="old_pass" required>
                                </div>
								@endif
								<div class="form-group">
                                    <label>New Password</label>
                                    <input class="form-control" placeholder="Choose new password" name="pass" required>
                                </div>
                                <button class="btn btn-success pull-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@endforeach	

	@foreach($admins as $user)
          <div id="edit{{$user->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit System User</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('editsysuser') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{$user->id}}">
                                <div class="form-group">
                                    <label>Profile Name</label>
                                    <input class="form-control" value="{{ $user->name }}" placeholder="Profile Name" name="name" required>
                                </div>
								<div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" value="{{ $user->username }}" placeholder="Username" disabled>
                                </div>
								<div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="{{ $user->email }}"  placeholder="Email" name="email" required>
                                </div>
								
                                <button class="btn btn-success pull-right">Edit User</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@endforeach	
@endsection

@section('extracss')
<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"/>
@endsection
@section('extrajs')
<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-default.demo.min.js')}}"></script>
@endsection
@section('extrainit')
TableManageDefault.init();
@endsection