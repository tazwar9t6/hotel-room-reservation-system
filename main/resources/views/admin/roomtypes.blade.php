@extends('admin.layout.head')
@section('content')
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Room Management</a></li>
				<li class="active">All Room Types</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Room Management <button type="button" data-toggle="modal" data-target="#addRoomtype" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New Room Type</button> </h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All Room Types</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" style="font-size:13px;font-weight: bold;" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Room Type</th>
                                        <th>Room Price</th>
                                        <th>Max Person</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								@foreach($types as $type)
                                        <td>{{$type->room_type}}</td>
                                        <td>{{$type->price}}</td>
                                        <td>{{$type->max_person}}</td>
										<td>
										<a data-toggle="modal" data-target="#edit{{$type->id}}" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-pencil"></i></a>
										<a onclick="confirm_click();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                               <script type="text/javascript">
										function confirm_click()
										{
											if (confirm("Are you sure?")) {
												event.preventDefault();
												document.getElementById('delete-form{{$type->id}}').submit();
												} else {
													return false;
													}
											}
										</script>
										<form id="delete-form{{$type->id}}" action="{{ route('removeroomtype') }}" method="post" style="display: none;">
										{{ csrf_field() }}
										<input type="hidden" name="id" value="{{$type->id}}">
										</form>
										</td>
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
				    <!-- Add Room Type -->
    <div id="addRoomtype" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Room Type</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('addroomtype') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
                                <div class="response"></div>
                                <div class="form-group">
                                    <label>Room Type</label>
									<input class="form-control" placeholder="Room type" name="roomtype"
                                           data-error="Enter Room type" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label>Room Price</label>
                                    <input class="form-control" placeholder="Room Price" name="roomprice"
                                           data-error="Enter Room Price" required>
                                    <div class="help-block with-errors"></div>
                                </div>
								
								<div class="form-group">
                                    <label>Max Person</label>
                                    <input class="form-control" placeholder="Max Person" name="maxperson"
                                           data-error="Enter Max Person" required>
                                    <div class="help-block with-errors"></div>
                                </div>
								
								
                                <button class="btn btn-success pull-right">Add Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@foreach($types as $type1)
	<div id="edit{{$type1->id}}" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Room Type</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="{{ route('editroomtype') }}" data-toggle="validator" method="post" role="form">
							{{ csrf_field() }}
							<input type="hidden" name="id" value="{{$type1->id}}">
                                <div class="response"></div>
                                <div class="form-group">
                                    <label>Room Type</label>
                                    <select class="form-control" name="room_type" required
                                            data-error="Select Room Type">
                                        <option selected disabled>Select Room Type</option>
										@php
										$room_types = \App\Roomtype::all();
										@endphp
										@foreach($room_types as $type)
										<option value="{{$type->room_type}}"@if ($type->id == $type1->id) selected @endif >{{$type->room_type}}</option>
										@endforeach
										</select>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label>Room Price</label>
                                    <input class="form-control" placeholder="Room Price" value="{{$type1->price}}" name="price"
                                           data-error="Enter Price" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label>Max Person</label>
                                    <input class="form-control" placeholder="Room No" value="{{$type1->max_person}}" name="max_person"
                                           data-error="Enter Room No" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <button class="btn btn-success pull-right">Update Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	@endforeach	
@endsection

@section('extracss')
<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"/>
@endsection
@section('extrajs')
<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-default.demo.min.js')}}"></script>
@endsection
@section('extrainit')
TableManageDefault.init();
@endsection