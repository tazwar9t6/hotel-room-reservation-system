@extends('admin.layout.head')
@section('content')
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Staff Management</a></li>
				<li class="active">Staff History</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Staff History <a class="btn btn-inverse"><i class="fa fa-user"></i> Name : {{$staffs->emp_name}}</a> <a class="btn btn-inverse"><i class="fa fa-address-book"></i> Contact : {{$staffs->contact_no}}</a> <a class="btn btn-inverse"><i class="fa fa-dollar"></i> Salery : {{$staffs->salary}}</a> </h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Staff History</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" style="font-size:13px;font-weight: bold;"  class="table table-striped table-bordered">
                                <thead>
                                    <tr>
									    <th>No</th>
                                        <th>Shift</th>
										<th>From Date</th>
										<th>To date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								@foreach($histories as $key => $history)
								        @php
									    $shift = \App\Shift::where('id',$history->shift_id)->first();
										$from_date = date('M j, Y', strtotime($history->from_date));
										$to_date = date('M j, Y', strtotime($history->to_date));
										@endphp
								        <td>{{$key+1}}</td>
                                        <td>{{$shift->shift}} - {{$shift->shift_timing}}</td>
										<td>{{$from_date}}</td>
										<td>@if($history->to_date == NULL) <span style="font-size:13px;font-weight: bold;" class="label label-success">Currently Working</span> @else {{$to_date}} @endif</td>
                                    </tr>
									@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
@endsection

@section('extracss')
<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"/>
@endsection
@section('extrajs')
<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/js/table-manage-default.demo.min.js')}}"></script>
@endsection
@section('extrainit')
TableManageDefault.init();
@endsection