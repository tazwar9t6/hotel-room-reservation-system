<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()
{
	if (Config::get('app.env') == 'install')
	{
		return redirect('installer');
	}
	else
	{
	return redirect('/login');
	}
});

Route::get('/404', function () {
    return view('404');
})->name('404');



Route::group(['prefix' => 'installer'], function () {
	
	Route::get('/', 'InstallController@Index')->name('installer');
	Route::get('/dbsetup', 'InstallController@DBForm')->name('dbinfo');
	Route::post('/dbsetup', 'InstallController@DBInfo')->name('up.dbinfo');
	Route::get('/setup', 'InstallController@AdminForm')->name('AdminForm');
	Route::post('/setup', 'InstallController@AdminSetup')->name('AdminSetup');
	
});

Route::get('/', 'ReservationController@Reservation');
Route::get('/reservation', 'ReservationController@Reservation')->name('public.reservation');
Route::post('/getperson', 'ReservationController@Getperson')->name('getperson');
Route::post('/getprice', 'ReservationController@Getprice')->name('public.getprice');
Route::post('/booking', 'ReservationController@Booking')->name('public.booking');


Auth::routes();
Route::group(['prefix' => 'admin'], function () {

  //Admin Auth
//  Route::get('/', 'Auth\LoginController@showLoginForm');
  Route::post('/login', 'Auth\LoginController@login')->name('admin.login');
  Route::post('/logout', 'Auth\LoginController@logout')->name('admin.logout');
  Route::get('/home', 'HomeController@Home')->name('home');
  Route::get('/allusers', 'HomeController@AllCus')->name('allusers');
  Route::get('/allroom', 'HomeController@Allrooms')->name('allroom');
  Route::get('/sysusers', 'HomeController@SysUsers')->name('sysusers');
  Route::get('/allstaff', 'HomeController@Allstaff')->name('allstaff');
  Route::get('/allstaff/history/{id}', 'HomeController@EmpHistory')->name('history');  
  Route::post('/editStaff', 'HomeController@EditStaff')->name('editStaff');
  Route::post('/removeStaff', 'HomeController@RemoveStaff')->name('removeStaff');
  Route::get('/staffPosition', 'HomeController@staffPosition')->name('staffPosition');
  Route::post('/addroomtype', 'HomeController@Addroomtype')->name('addroomtype');
  Route::post('/addroom', 'HomeController@Addroom')->name('addroom');
  Route::post('/addemp', 'HomeController@AddEmp')->name('addemp');
  Route::post('/changeShift', 'HomeController@changeShift')->name('changeShift');
  Route::post('/editstafftype', 'HomeController@Editstafftype')->name('editstafftype');
  Route::post('/addPositioin', 'HomeController@addPositioin')->name('addPositioin');
  Route::get('/allShift', 'HomeController@allShift')->name('allShift');
  Route::post('/addShift', 'HomeController@addShift')->name('addShift');
  Route::post('/editShift', 'HomeController@editShift')->name('editShift');
  Route::post('/addsysuser', 'HomeController@AddSysUser')->name('addsysuser');
  Route::post('/removeShift', 'HomeController@removeShift')->name('removeShift');
  Route::post('/RemoveSysUser', 'HomeController@RemoveSysUser')->name('RemoveSysUser');
  Route::get('/Idcards', 'HomeController@IdCards')->name('Idcards');
  Route::post('/addIdcard', 'HomeController@AddIdCard')->name('addIdcard');
  Route::post('/editIdcard', 'HomeController@EditIdcard')->name('editIdcard');
  Route::post('/removeIdcard', 'HomeController@RemoveIdCard')->name('removeIdcard');
  Route::post('/removeCus', 'HomeController@removeCus')->name('removeCus');
  Route::get('/roomtypes', 'HomeController@Roomtypes')->name('roomtypes');
  Route::post('/removeroomtype', 'HomeController@RemoveRoomtypes')->name('removeroomtype');
  Route::post('/removeroom', 'HomeController@RemoveRoom')->name('removeroom');
  Route::post('/RemovePosition', 'HomeController@RemovePosition')->name('RemovePosition');
  Route::post('/editroomtype', 'HomeController@Editroomtype')->name('editroomtype');
  Route::post('/editroom', 'HomeController@Editroom')->name('editroom');
  Route::get('/reservation', 'HomeController@Reservation')->name('reservation');
  Route::get('/reqreservation', 'HomeController@ReservationReq')->name('reqreservation');
  Route::post('/removebookingreq', 'HomeController@ReservationReqRemove')->name('removebookingreq');
  Route::post('/getrooms', 'HomeController@Getrooms')->name('getrooms');
  Route::post('/getprice', 'HomeController@Getprice')->name('getprice');
  Route::post('/bookingDetails', 'HomeController@bookingDetails')->name('bookingDetails');
  Route::post('/cusDetails', 'HomeController@cusDetails')->name('cusDetails');
  Route::post('/checkInRoom', 'HomeController@checkInRoom')->name('checkInRoom');
  Route::post('/checkOutRoom', 'HomeController@checkOutRoom')->name('checkOutRoom');
  Route::post('/checkIntoday', 'HomeController@checkIntoday')->name('checkIntoday');
  Route::post('/checkOuttoday', 'HomeController@checkOuttoday')->name('checkOuttoday');
  Route::post('/checkOutBooking', 'HomeController@checkOutBooking')->name('checkOutBooking');
  Route::post('/advancePayment', 'HomeController@advancePayment')->name('advancePayment');
  Route::post('/cutomerDetails', 'HomeController@cutomerDetails')->name('cutomerDetails');
  Route::post('/syschngpass', 'HomeController@SysChngPass')->name('syschngpass');
  Route::post('/editsysuser', 'HomeController@EditSysUser')->name('editsysuser');
  Route::post('/booking', 'HomeController@Booking')->name('booking');
  Route::post('/booking/{booking}', 'HomeController@Booking1')->name('req.booking');
  Route::get('/reservation/{type}/{room}', 'HomeController@Reservation2')->name('book');
  Route::get('/reservation/{booking}/{type}/{customer}', 'HomeController@Reservation3')->name('req.book');
  Route::get('/invoice/{id}', 'HomeController@showPDFInvoice')->name('showinvoice');
  Route::get('/invoice', 'HomeController@Invoice')->name('invoice');
  Route::get('/showInvoice/{id}', 'HomeController@showInvoice1')->name('showInvoice1');
  Route::post('/InvoiceUpdate', 'HomeController@InvoiceUpdate')->name('invoiceup');
  
});