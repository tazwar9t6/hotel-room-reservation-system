<?php $__env->startSection('content'); ?>
<div class="container">
			<div id="wizard_container">
					<div id="middle-wizard">
							<div class="submit step" id="end">
								<div class="question_title">
									<h3>Hotel Management Installer</h3>
									<p>Software Requirements</p>
								</div>
								<div class="row justify-content-center">
								<div class="col-lg-6">
								<table class="table table-striped">
								<tbody>
								<?php
								$error = 0;
								$phpversion = version_compare(PHP_VERSION, '7.0.0', '>=');
								if ($phpversion==true) {
									$error = $error+0;
									echo createTable("PHP", "Required PHP version 7.0 or higher",1);
									}else{
										$error = $error+1;
										echo createTable("PHP", "Required PHP version 7.0 or higher",0);
										}
										?>
							<?php $__currentLoopData = $extensions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php
							$extension = extension_check($key);
							if ($extension==true) {
								$error = $error+0;
								echo createTable($key, "Required ".strtoupper($key)." PHP Extension",1);
								}else{
									$error = $error+1;
								echo createTable($key, "Required ".strtoupper($key)." PHP Extension",0);
									}
							?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							
							<?php $__currentLoopData = $folders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php
							$folder_perm = folder_permission($key);
							if ($folder_perm==true) {
								$error = $error+0;
								echo createTable(str_replace("../", "", $key)," Required permission: 0775 ",1);
								}else{
									$error = $error+1;
									echo createTable(str_replace("../", "", $key)," Required permission: 0775 ",0);
									}
							?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php
							$envCheck = is_writable('main/.env');
							if ($envCheck==true) {
								$error = $error+0;
								echo createTable('env'," Required .env to be writable",1);
								}else{
									$error = $error+1;
									echo createTable('env'," Required .env to be writable",0);
									}						
							?>
  </tbody>
  </table>
  </div>
  </div>
								</div>
							</div>
						</div>
					<div id="bottom-wizard">
							<?php if($error == 0): ?>
								<button onclick="next()" class="btn btn-info anchor">Next Step <i class="fa fa-angle-double-right"></i></button>
							<?php else: ?>
								<button onclick="refresh()" class="btn btn-info anchor">ReCheck <i class="fa fa-sync-alt"></i></button>
							<?php endif; ?>
							</div>

</div>
<script>
function refresh() {
   setTimeout(function () {
        location.reload()
    }, 100);
}
function next() {
   setTimeout(function () {
        window.location.replace("<?php echo e(route('dbinfo')); ?>")
    }, 100);
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('install.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>