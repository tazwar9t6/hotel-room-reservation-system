<?php if(session('success')): ?>
<div class="alert alert-success fade in m-b-15">
<strong>Success!</strong> <?php echo e(session('success')); ?> <span class="close" data-dismiss="alert">×</span>
</div>
<?php endif; ?>
<?php if(session('alert')): ?>
<div class="alert alert-warning fade in m-b-15">
<strong>Alert!</strong> <?php echo e(session('alert')); ?> <span class="close" data-dismiss="alert">×</span>
</div>
<?php endif; ?>
<?php if($errors->any()): ?>
<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="alert alert-warning fade in m-b-15">
<strong>Sorry!</strong> <?php echo e($error); ?> <span class="close" data-dismiss="alert">×</span>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>