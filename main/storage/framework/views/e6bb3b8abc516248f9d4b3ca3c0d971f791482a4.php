<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<div class="image">
							<a><img src="<?php echo e(asset('assets/img/user-13.jpg')); ?>" alt="" /></a>
						</div>
						<div class="info">
							<?php echo e($admin->name); ?>

							<small><?php if($admin->type == "admin"): ?>Administrator <?php else: ?> <?php echo e(ucfirst($admin->type)); ?> <?php endif; ?></small>
						</div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Navigation</li>
					<li class="<?php if(request()->path() == 'admin/home'): ?> active <?php endif; ?>"><a href="<?php echo e(route('home')); ?>"><i class="fa fa-laptop"></i> <span>Dashboard</span></a></li>
					<li class="has-sub <?php if(request()->path() == 'admin/allroom'): ?> active 
							<?php elseif(request()->path() == 'admin/roomtypes'): ?> active
							<?php elseif(request()->path() == 'admin/reservation'): ?> active
							<?php elseif(request()->is('admin/reservation/*/*')): ?> active
							<?php endif; ?>">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-bed"></i>
						    <span>Room Management</span> 
						</a>
                    <ul class="sub-menu">					
					<li class="<?php if(request()->path() == 'admin/allroom'): ?> active <?php endif; ?>"><a href="<?php echo e(route('allroom')); ?>">All Rooms</a></li>
					<li class="<?php if(request()->path() == 'admin/reservation'): ?> active <?php endif; ?> <?php if(request()->is('admin/reservation/*/*')): ?> active <?php endif; ?>"><a href="<?php echo e(route('reservation')); ?>">Reservation</a></li>
					<li class="<?php if(request()->path() == 'admin/roomtypes'): ?> active <?php endif; ?>"><a href="<?php echo e(route('roomtypes')); ?>">Room Types</a></li>
					</ul>
					</li>
					<?php if($admin->type == "admin"): ?>
					<li class="has-sub <?php if(request()->path() == 'admin/allstaff'): ?> active 
							<?php elseif(request()->path() == 'admin/staffPosition'): ?> active
							<?php elseif(request()->path() == 'admin/allShift'): ?> active
							<?php elseif(request()->is('admin/allstaff/history/*')): ?> active
							<?php endif; ?>">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-users"></i>
						    <span>Staff Management</span> 
						</a>
                    <ul class="sub-menu">					
					<li class="<?php if(request()->path() == 'admin/allstaff'): ?> active <?php endif; ?> <?php if(request()->is('admin/allstaff/history/*')): ?> active <?php endif; ?>"><a href="<?php echo e(route('allstaff')); ?>">All Staff</a></li>
					<li class="<?php if(request()->path() == 'admin/staffPosition'): ?> active <?php endif; ?>"><a href="<?php echo e(route('staffPosition')); ?>">Staff Positions</a></li>
					<li class="<?php if(request()->path() == 'admin/allShift'): ?> active <?php endif; ?>"><a href="<?php echo e(route('allShift')); ?>">Manage Shift</a></li>
					
					</ul>
					</li>
					<?php endif; ?>
					<li class="<?php if(request()->path() == 'admin/allusers'): ?> active <?php endif; ?>"><a href="<?php echo e(route('allusers')); ?>"><i class="fa fa-smile-o"></i> <span>All Customers</span></a></li>
					<?php if($admin->type == "admin"): ?>
					<li class="<?php if(request()->path() == 'admin/Idcards'): ?> active <?php endif; ?>"><a href="<?php echo e(route('Idcards')); ?>"><i class="fa fa-credit-card"></i> <span>Manage ID Card Types</span></a></li>
				    <?php endif; ?>
					<li class="<?php if(request()->path() == 'admin/sysusers'): ?> active <?php endif; ?>"><a href="<?php echo e(route('sysusers')); ?>"><i class="fa fa-user"></i> <span>System Users</span></a></li>
					<li class="<?php if(request()->path() == 'admin/invoice'): ?> active <?php endif; ?>"><a href="<?php echo e(route('invoice')); ?>"><i class="fa fa-print"></i> <span>Invoice Settings</span></a></li>
			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>