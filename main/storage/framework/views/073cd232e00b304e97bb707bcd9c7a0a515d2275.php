<?php $__env->startSection('content'); ?>

<h1 align="center" class="page-header">New Reservation</h1>

    <div class="row">
			    <!-- begin col-12 -->
				<div class="col-md-2"></div>
			    <div class="col-md-8">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                            <h4 class="panel-title">Room Information</h4>
                        </div>
					<div class="panel-body">
            <form role="form" id="booking" autocomplete="off">
                <div class="col-lg-12">

							<div class="row">
							<div class="col-6 col-md-4">
							<div class="widget widget-stats bg-black">
							<div class="stats-info">
							<h4 style="font-weight: bold; font-size:15px;">TOTAL DAYS : 
							<span id="staying_day">0</span> Days</h4>
							</div>
							</div>
							</div>
							<div class="col-6 col-md-4">
							<div class="widget widget-stats bg-black">
							<div class="stats-info">
							<h4 style="font-weight: bold; font-size:15px;">ROOM PRICE : 
							<span id="price">0</span> /-</h4>
							</div>
							</div>
							</div>
							<div class="col-6 col-md-4">
							<div class="widget widget-stats bg-black">
							<div class="stats-info">
							<h4 style="font-weight: bold; font-size:15px;">TOTAL AMOUNT : 
							<span id="total_price">0</span> /-</h4>
							</div>
							</div>
							</div>
							</div>
							
                                <div class="form-group col-lg-6">
                                    <label>Room Type</label>
									<select class="form-control" id="room_type" onchange="get_person(this.value);" required>
                                        <option selected disabled>Select Room Type</option>
                                        <?php $__currentLoopData = $room_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($type->id); ?>"><?php echo e($type->room_type); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Total person</label>
                                    <select class="form-control" id="total_person" required>
                                        <option selected disabled>Select person</option>
										</select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check In Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_in_date"  required>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Check Out Date</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="check_out_date" required>
                                </div>
                            </div>
                        </div>
						</div>
						</div>
                        </div>
						
						
						
						<div class="row">
						<div class="col-md-2"></div>
			    <div class="col-md-8">
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>
                            <h4 class="panel-title">Customer Detail</h4>
                        </div>
                        <div class="panel-body">
						<div class="col-lg-12">
						<div class="col-lg-3"></div>
						<div class="form-group col-lg-6">
                                    <select class="form-control input-lg" id="customer" required>
										<option value="new" selected>New customer</option>
										<option value="old">Old customer</option>
										</select>
                                </div>
								
							<div id="old_customer" style="display:none;">
							<div class="form-group col-lg-12">
                                    <label>Contact No</label>
                                    <input type="number" class="form-control" placeholder="Contact No" id="contact_no">
                                </div>
							</div>
							<div id="customer_details">
                            <div class="form-group col-lg-6">
                                   <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="First Name" id="first_name">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Last Name</label>
                                     <input class="form-control" type="text" placeholder="Last Name" id="last_name">
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Contact No</label>
                                    <input type="number" class="form-control" placeholder="Contact No" id="contact_no">
                                </div>

								<div class="form-group col-lg-6">
                                     <label>Email Address</label>
                                    <input type="text" class="form-control" placeholder="Email Address" id="email">
                                </div>

                                <div class="form-group col-lg-6">
                                   <label>ID Card Type</label>
                                    <select class="form-control" id="id_card_type" required>
                                    <option selected disabled>Select ID Card Type</option>
                                        <?php $__currentLoopData = $idcards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $card): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($card->id); ?>"><?php echo e($card->id_card_type); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>ID Card No</label>
                                     <input type="text" class="form-control" placeholder="ID Card No" id="id_card_no">
                                </div>
								
								
                            <div class="form-group col-lg-12">
                                <label>Address</label>
                                <textarea class="form-control" rows="3" id="address"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
							</div>
							</div>
                        </div>
                    </div>
				</div>
				</div>
				<div class="form-group text-center">
                <button type="submit" id="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Please wait" class="btn btn-primary btn-lg m-r-6">Submit</button>
				</div>
            </form>
<div id="bookingConfirm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Room Booking</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div style="font-size:15px;" class="alert bg-success alert-dismissable" role="alert"><em class="fa fa-lg fa-check-circle">&nbsp;</em><b>Reservation request successfully send</b></div>
                        <table style="font-size:13px;font-weight: bold;" class="table table-striped table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Customer Name</td>
                                <td id="getCustomerName"></td>
                            </tr>
                            <tr>
                                <td>Room Type</td>
                                <td id="getRoomType"></td>
                            </tr>
                            <tr>
                                <td>Check In</td>
                                <td id="getCheckIn"></td>
                            </tr>
                            <tr>
                                <td>Check Out</td>
                                <td id="getCheckOut"></td>
                            </tr>
                            <tr>
                                <td>Total Amount</td>
                                <td id="getTotalPrice"></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" data-dismiss="modal">Okay</a>
            </div>
        </div>
    </div>
</div>

<script>
    var booking = "<?php echo e(route('public.booking')); ?>";
	var token = "<?php echo e(csrf_token()); ?>";
	
	
	function get_person(val) {
    $.ajax({
        type: 'post',
        url: '<?php echo e(route('getperson')); ?>',
        data: {
			_token : '<?php echo e(csrf_token()); ?>',
            id: val
        },
        success: function (response) {
            $('#total_person').html(response);
        }
    });
	get_price(val);
	}
	
	function get_price(val) {
    $.ajax({
        type: 'post',
        url: '<?php echo e(route('public.getprice')); ?>',
        data: {
			_token : '<?php echo e(csrf_token()); ?>',
            id: val
        },
        success: function (response) {
            $('#price').html(response);
            var days = document.getElementById('staying_day').innerHTML;
            $('#total_price').html(response*days);
        }
    });
	}
	
</script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('extracss'); ?>
<link href="<?php echo e(asset('assets/css/foundation-datepicker.css')); ?>" rel="stylesheet" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrajs'); ?>
<script src="<?php echo e(asset('assets/js/foundation-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/bookingReq.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.public', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>