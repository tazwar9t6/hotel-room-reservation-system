<?php $__env->startSection('content'); ?>
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Shift Management</a></li>
				<li class="active">All Shift</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Shift Management <button type="button" data-toggle="modal" data-target="#addShift" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New Shift</button> </h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All Shifts</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" style="font-size:13px;font-weight: bold;"  class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
										<th>Shift</th>
                                        <th>Time</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								<?php $__currentLoopData = $shifts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $no=>$shift): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td><?php echo e($no+1); ?></td>
                                        <td><?php echo e($shift->shift); ?></td>
										<td><?php echo e($shift->shift_timing); ?></td>
										<td>
										<a data-toggle="modal" data-target="#edit<?php echo e($shift->id); ?>" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-pencil"></i></a>
										<a onclick="confirm_click();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                               <script type="text/javascript">
										function confirm_click()
										{
											if (confirm("Are you sure?")) {
												event.preventDefault();
												document.getElementById('delete-form<?php echo e($shift->id); ?>').submit();
												} else {
													return false;
													}
											}
										</script>
										<form id="delete-form<?php echo e($shift->id); ?>" action="<?php echo e(route('removeShift')); ?>" method="post" style="display: none;">
										<?php echo e(csrf_field()); ?>

										<input type="hidden" name="id" value="<?php echo e($shift->id); ?>">
										</form>
										</td>
                                    </tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
				    <!-- Add Room Type -->
	<div id="addShift" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Staff Position</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('addShift')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label>Shift</label>
                                    <input class="form-control" placeholder="Shift" name="shift" required>
                                </div>
								<div class="form-group">
                                    <label>Shift Timing</label>
                                    <input class="form-control" placeholder="Shift Timing" name="timing" required>
                                </div>
                                <button class="btn btn-success pull-right">Add Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<?php $__currentLoopData = $shifts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shift): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div id="edit<?php echo e($shift->id); ?>" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Shift</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('editShift')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

							<input type="hidden" name="id" value="<?php echo e($shift->id); ?>">
                                <div class="form-group">
                                    <label>Shift</label>
                                    <input class="form-control" placeholder="Shift" value="<?php echo e($shift->shift); ?>" name="shift" required>
                                </div>
								<div class="form-group">
                                    <label>Shift Timing</label>
                                    <input class="form-control" placeholder="Shift Timing" value="<?php echo e($shift->shift_timing); ?>" name="timing" required>
                                </div>
                                <button class="btn btn-success pull-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('extracss'); ?>
<link href="<?php echo e(asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrajs'); ?>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/table-manage-default.demo.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrainit'); ?>
TableManageDefault.init();
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>