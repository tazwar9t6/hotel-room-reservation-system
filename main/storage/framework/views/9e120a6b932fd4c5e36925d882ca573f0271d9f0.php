<?php $__env->startSection('content'); ?>
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">All Customers</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">All Customers</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All Customers</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table"  style="font-size:13px;font-weight: bold;" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Contact No</th>
                                        <th>Email</th>
                                        <th>Check In</th>
                                        <th>Check Out</th>
										<th>Action</th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php
								$date = \App\Booking::where('customer_id',$cus->id)->first();
								$in = date('M j, Y - g:i A', strtotime($date->check_in));
								$out = date('M j, Y - g:i A', strtotime($date->check_out));
								?>
                                        <td><?php echo e($cus->customer_name); ?></td>
                                        <td><?php echo e($cus->contact_no); ?></td>
                                        <td><?php echo e($cus->email); ?></td>
                                        <td><?php echo e($in); ?></td>
										<td><?php echo e($out); ?></td>
										<td><a target="_blank" href="<?php echo e(route('showinvoice', $cus->id)); ?>" class="btn btn-info btn-icon btn-circle btn-lg"><i class="fa fa-print"></i></a>
										<a data-toggle="modal" data-target="#cutomerDetail" data-id="<?php echo e($cus->id); ?>" id="usreDetails" class="btn btn-info btn-icon btn-circle btn-lg"><i class="fa fa-eye"></i></a>
										<?php if($admin->type == "admin"): ?>
										<a onclick="confirm_click<?php echo e($cus->id); ?>();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                                <script type="text/javascript">
										function confirm_click<?php echo e($cus->id); ?>()
										{
											if (confirm("Are you sure to remove this customer details?")) {
												event.preventDefault();
												document.getElementById('delete-form<?php echo e($cus->id); ?>').submit();
												} else {
													return false;
													}
											}
											</script>
										<form id="delete-form<?php echo e($cus->id); ?>" action="<?php echo e(route('removeCus')); ?>" method="post" style="display: none;">
										<?php echo e(csrf_field()); ?>

										<input type="hidden" name="id" value="<?php echo e($cus->id); ?>">
										</form>
										<?php endif; ?>
										</td>
										
										
                                    </tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
<div id="cutomerDetail" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Customer Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table style="font-size:13px;font-weight: bold;" class="table table-responsive table-bordered">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td id="customer_name"></td>
                                </tr>
                                <tr>
                                    <td>Contact Number</td>
                                    <td id="customer_contact_no"></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td id="customer_email"></td>
                                </tr>
                                <tr>
                                    <td>ID Card Type</td>
                                    <td id="customer_id_card_type"></td>
                                </tr>
                                <tr>
                                    <td>ID Card Number</td>
                                    <td id="customer_id_card_number"></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td id="customer_address"></td>
                                </tr>
								<tr>
                                    <td>Checked In</td>
                                    <td id="check_id"></td>
                                </tr>
                                <tr>
                                    <td>Checked Out</td>
                                    <td id="check_out"></td>
                                </tr>
								<tr>
                                    <td>Total Cost</td>
                                    <td id="total"></td>
                                </tr>
								<tr>
                                    <td>Advance on Checked In</td>
                                    <td id="advance"></td>
                                </tr>
								<tr>
                                    <td>Status</td>
                                    <td id="status"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
				<div class="modal-footer">
											<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
										</div>
            </div>
        </div>
    </div>
	
<script>
    var usreDetails = "<?php echo e(route('usreDetails')); ?>";
	var token = "<?php echo e(csrf_token()); ?>";
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('extracss'); ?>
<link href="<?php echo e(asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrajs'); ?>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/table-manage-default.demo.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/ajax.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrainit'); ?>
TableManageDefault.init();
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>