<?php $__env->startSection('content'); ?>
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li class="active">Invoice Settings</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Invoice Settings</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			      <div class="col-md-8">
			        <!-- begin panel -->
                    <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Invoice Data</h4>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" action="<?php echo e(route('invoiceup')); ?>" method="post" role="form">
							<?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Hotel name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="<?php echo e($invoice->name); ?>" name="name" placeholder="Hotel Name" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Hotel Address</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="<?php echo e($invoice->address); ?>" name="address" placeholder="Hotel Address" />
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="email" class="form-control" value="<?php echo e($invoice->email); ?>" name="email" placeholder="Email" />
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <label class="col-md-3 control-label">Contact</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="<?php echo e($invoice->phone); ?>" name="number" placeholder="Contact Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Invoice Watermark</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="<?php echo e($invoice->watermark); ?>" name="watermark" placeholder="Invoice Watermark" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Invoice Currency</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="<?php echo e($invoice->currency); ?>" name="currency" placeholder="Currency" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Invoice Footer</label>
                                    <div class="col-md-9">
                                        <textarea name="footer" class="form-control" placeholder="Invoice Footer" rows="5"><?php echo e($invoice->footer); ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9">
                                        <button type="submit" class="btn btn-sm btn-success">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-6 -->
                <!-- begin col-6 -->
            </div>
			
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>