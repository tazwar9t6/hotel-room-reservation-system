<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $__env->yieldContent('title'); ?></title>

        <!-- Styles -->
<style class="cp-pen-styles">@import  url("https://fonts.googleapis.com/css?family=Roboto+Mono");
.center-xy {
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  position: absolute;
}

html, body {
  font-family: 'Roboto Mono', monospace;
  font-size: 16px;
}

html {
  box-sizing: border-box;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

body {
  background-color: #000;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.container {
  width: 100%;
}

.copy-container {
  text-align: center;
}

p {
  color: #fff;
  font-size: 24px;
  letter-spacing: .2px;
  margin: 0;
}

#cb-replay {
  fill: #666;
  width: 20px;
  margin: 15px;
  right: 0;
  bottom: 0;
  position: absolute;
  overflow: inherit;
  cursor: pointer;
}
#cb-replay:hover {
  fill: #888;
}
</style>
</head>
<body>
<div class="container">
  <div class="copy-container center-xy">
    <p>
      <?php echo $__env->yieldContent('message'); ?>
    </p>
  </div>
</div>
</body>
</html>