<?php $__env->startSection('content'); ?>
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">System User Management</a></li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">System Users <?php if($admin->type == "admin"): ?><button type="button" data-toggle="modal" data-target="#addsysuser" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New System User</button><?php endif; ?> </h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All Users</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" style="font-size:13px;font-weight: bold;"  class="table table-striped table-bordered">
                                <thead>
                                    <tr>
										<th>Profile name</th>
                                        <th>Username</th>
										<th>Email</th>
										<th>Role</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								<?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td><?php echo e($user->name); ?></td>
                                        <td><?php echo e($user->username); ?></td>
										<td><?php echo e($user->email); ?></td>
										<td><?php if($user->type == "admin"): ?>Administrator <?php else: ?> <?php echo e(ucfirst($user->type)); ?> <?php endif; ?></td>
										<td>
										<a data-toggle="modal" data-target="#passwd<?php echo e($user->id); ?>" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-key"></i></a>
										<a data-toggle="modal" data-target="#edit<?php echo e($user->id); ?>" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-pencil"></i></a>
										
										<?php if($user->id == $admin->id): ?>
										<?php else: ?>
										<a onclick="confirm_click();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                               <script type="text/javascript">
										function confirm_click()
										{
											if (confirm("Are you sure?")) {
												event.preventDefault();
												document.getElementById('delete-form<?php echo e($user->id); ?>').submit();
												} else {
													return false;
													}
											}
										</script>
										<form id="delete-form<?php echo e($user->id); ?>" action="<?php echo e(route('RemoveSysUser')); ?>" method="post" style="display: none;">
										<?php echo e(csrf_field()); ?>

										<input type="hidden" name="id" value="<?php echo e($user->id); ?>">
										</form>
										<?php endif; ?>
										</td>
                                    </tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
				    <!-- Add Room Type -->
	<?php if($admin->type == "admin"): ?>
    <div id="addsysuser" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New System User</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('addsysuser')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label>Profile Name</label>
                                    <input class="form-control" placeholder="Profile Name" name="name" required>
                                </div>
								<div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" placeholder="Username" name="user" required>
                                </div>
								<div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" placeholder="Email" name="email" required>
                                </div>
								<div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" placeholder="Password" name="pass" required>
                                </div>
								<div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control" name="role" required>
									<option selected disabled>Select User Role</option>
									<option value="1">Administrator</option>
									<option value="0">Manager</option>
									</select>
                                </div>
								
                                <button class="btn btn-success pull-right">Add User</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<?php endif; ?>
	<?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div id="passwd<?php echo e($user->id); ?>" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change System User Password</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('syschngpass')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

							<input type="hidden" name="id" value="<?php echo e($user->id); ?>">
                                <?php if($user->id == $admin->id): ?>
								<div class="form-group">
                                    <label>Old Password</label>
                                    <input class="form-control" placeholder="Old password" name="old_pass" required>
                                </div>
								<?php endif; ?>
								<div class="form-group">
                                    <label>New Password</label>
                                    <input class="form-control" placeholder="Choose new password" name="pass" required>
                                </div>
                                <button class="btn btn-success pull-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	

	<?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div id="edit<?php echo e($user->id); ?>" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit System User</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('editsysuser')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

							<input type="hidden" name="id" value="<?php echo e($user->id); ?>">
                                <div class="form-group">
                                    <label>Profile Name</label>
                                    <input class="form-control" value="<?php echo e($user->name); ?>" placeholder="Profile Name" name="name" required>
                                </div>
								<div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" value="<?php echo e($user->username); ?>" placeholder="Username" disabled>
                                </div>
								<div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" value="<?php echo e($user->email); ?>"  placeholder="Email" name="email" required>
                                </div>
								
                                <button class="btn btn-success pull-right">Edit User</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
<?php $__env->stopSection(); ?>

<?php $__env->startSection('extracss'); ?>
<link href="<?php echo e(asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrajs'); ?>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/table-manage-default.demo.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrainit'); ?>
TableManageDefault.init();
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>