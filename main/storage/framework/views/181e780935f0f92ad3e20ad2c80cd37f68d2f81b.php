<?php $__env->startSection('content'); ?>
		<div class="container">
			<div id="wizard_container">
				<form action="<?php echo e(route('verificationchecker')); ?>" method="post" role="form">
				<?php echo e(csrf_field()); ?>

					<div id="middle-wizard">
							<div class="submit step" id="end">
								<div class="question_title">
									<h3>Hotel Management Installer</h3>
									<p>Purchase Verification</p>
								</div>
								<?php if(session('alert')): ?>
								<div style="display: block;margin-left:auto;margin-right:auto;" class="alert alert-danger col-md-4 col-md-offset-5" align="center">
								<?php echo e(session('alert')); ?>

								</div>
								<?php endif; ?>
								<?php if($errors->any()): ?>
									<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div style="display: block;margin-left:auto;margin-right:auto;" class="alert alert-danger col-md-4 col-md-offset-5" align="center">
								<?php echo e($error); ?>

								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<div class="row justify-content-center">
								
									<div class="col-lg-5">
										<div class="box_general">
											<div class="form-group">
												<input type="text" name="item_id" class=" form-control" placeholder="Item ID">
											</div>
											<div class="form-group">
												<input type="text" name="purchase_code" class=" form-control" placeholder="Item Purchase Code">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="bottom-wizard">
						<button class="btn btn-info anchor">Next Step <i class="fa fa-angle-double-right"></i></button>
					</div>
				</form>
			</div>
		</div>										
<?php $__env->stopSection(); ?>
<?php echo $__env->make('install.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>