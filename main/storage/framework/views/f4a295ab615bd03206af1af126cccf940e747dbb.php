<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Room Management System Dashboard</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/css/animate.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/css/style.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/css/style-responsive.min.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/css/theme/default.css')); ?>" rel="stylesheet" id="theme" />
	<link href="<?php echo e(asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.css')); ?>" rel="stylesheet" />
	<link href="<?php echo e(asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/plugins/gritter/css/jquery.gritter.css')); ?>" rel="stylesheet" />
	<?php echo $__env->yieldContent('extracss'); ?>
	<script src="<?php echo e(asset('assets/plugins/pace/pace.min.js')); ?>"></script>
	<link rel="icon" href="<?php echo e(asset('assets/img/icon.png')); ?>" type="image/x-icon" />
</head>


<body>
	<div id="page-loader" class="fade in"><span class="spinner"></span></div></div>

<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">

<div id="header" class="header navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand"><span class="navbar-logo"></span> <b style="font-size:15px;">Room Management</b> </a>
					<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown navbar-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?php echo e(asset('assets/img/user-13.jpg')); ?>" alt="" /> 
							<span class="hidden-xs"><?php echo e($admin->name); ?></span> <b class="caret"></b>
						</a>
						<ul class="dropdown-menu animated fadeInLeft">
							<li class="arrow"></li>
							<li><a href="<?php echo e(route('sysusers')); ?>">Edit Profile</a></li>
							<li class="divider"></li>
							<li><a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">Log Out</a></li>
		<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
        <?php echo e(csrf_field()); ?>

      </form>
						</ul>
					</li>
				</ul>
			</div>
		</div>
<?php echo $__env->make('admin.layout.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="content" class="content">
<?php echo $__env->make('admin.layout.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldContent('content'); ?>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

	<script src="<?php echo e(asset('assets/plugins/jquery/jquery-1.9.1.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/jquery/jquery-migrate-1.1.0.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/bootstrap/js/bootstrap.min.js')); ?>"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo e(asset('assets/crossbrowserjs/html5shiv.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/crossbrowserjs/respond.min.js')); ?>"></script>
		<script src="<?php echo e(asset('assets/crossbrowserjs/excanvas.min.js')); ?>"></script>
	<![endif]-->
	<script src="<?php echo e(asset('assets/plugins/slimscroll/jquery.slimscroll.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/jquery-cookie/jquery.cookie.js')); ?>"></script>
	<?php echo $__env->yieldContent('extrajs'); ?>
	<script src="<?php echo e(asset('assets/plugins/gritter/js/jquery.gritter.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/flot/jquery.flot.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/flot/jquery.flot.time.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/flot/jquery.flot.resize.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/flot/jquery.flot.pie.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/sparkline/jquery.sparkline.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/js/dashboard.min.js')); ?>"></script>
	<script src="<?php echo e(asset('assets/js/apps.min.js')); ?>"></script>	
	<script>
		$(document).ready(function() {
			App.init();
			<?php echo $__env->yieldContent('extrainit'); ?>
		});
	</script>
</body>
</html>
