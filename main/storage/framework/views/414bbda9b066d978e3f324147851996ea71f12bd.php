<?php $__env->startSection('content'); ?>
		<div class="container">
			<div id="wizard_container">
				<form action="<?php echo e(route('AdminSetup')); ?>" method="post" role="form">
				<?php echo e(csrf_field()); ?>

					<div id="middle-wizard">
							<div class="submit step" id="end">
								<div class="question_title">
									<h3>Hotel Management Installer</h3>
									<p>Administrator Details</p>
								</div>
								<?php if(session('alert')): ?>
								<div style="display: block;margin-left:auto;margin-right:auto;" class="alert alert-danger col-md-4 col-md-offset-5" align="center">
								<?php echo e(session('alert')); ?>

								</div>
								<?php endif; ?>
								<?php if($errors->any()): ?>
									<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div style="display: block;margin-left:auto;margin-right:auto;" class="alert alert-danger col-md-4 col-md-offset-5" align="center">
								<?php echo e($error); ?>

								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<div class="row justify-content-center">
								
									<div class="col-lg-5">
										<div class="box_general">
											<div class="form-group">
												<input type="text" name="admin_name" class=" form-control" placeholder="Admin name" required>
											</div>
											<div class="form-group">
												<input type="text" name="admin_username" class=" form-control" placeholder="Admin Username" required
											</div>
											<div class="form-group">
												<input type="text" name="admin_password" class=" form-control" placeholder="Admin Password"required>
											</div>
											<div class="form-group">
												<input type="email" name="admin_email" class=" form-control" placeholder="Admin Email" required>
											</div>
											<div class="form-group add_bottom_30">
												<div class="styled-select">
													<select name="timezone" required>
														<option value="" selected>Select your Timezone</option>
														<?php $__currentLoopData = timezone_identifiers_list(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $timezone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
														<option value="<?php echo e($timezone); ?>"><?php echo e($timezone); ?></option>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="bottom-wizard">
						<button class="btn btn-info anchor">Submit <i class="fa fa-check"></i></button>
					</div>
				</form>
			</div>
		</div>
		<?php if(session('success')): ?>
    <div id="success-modal" class="modal modal-message modal-success fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                   <i style="display: block;margin-left:auto;margin-right:auto;" class="fa fa-check"></i>
                </div>
                <div class="modal-title">Success</div>
                <div class="modal-body"><?php echo e(session('success')); ?></div>
                <div class="modal-footer">
                    <a href="<?php echo e(route('admin.login')); ?>" class="btn btn-info anchor">Login to Admin Panel</a>
                </div>
            </div> <!-- / .modal-content -->
        </div> <!-- / .modal-dialog -->
    </div>
	<?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('install.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>