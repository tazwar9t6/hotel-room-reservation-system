<?php $__env->startSection('content'); ?>
		<div class="container">
			<div id="wizard_container">
				<form action="<?php echo e(route('up.dbinfo')); ?>" method="post" role="form">
				<?php echo e(csrf_field()); ?>

					<div id="middle-wizard">
							<div class="submit step" id="end">
								<div class="question_title">
									<h3>Hotel Management Installer</h3>
									<p>Database Details</p>
								</div>
								<?php if(session('alert')): ?>
								<div style="display: block;margin-left:auto;margin-right:auto;" class="alert alert-danger col-md-4 col-md-offset-5" align="center">
								<?php echo e(session('alert')); ?>

								</div>
								<?php endif; ?>
								<?php if($errors->any()): ?>
									<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div style="display: block;margin-left:auto;margin-right:auto;" class="alert alert-danger col-md-4 col-md-offset-5" align="center">
								<?php echo e($error); ?>

								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<div class="row justify-content-center">
								
									<div class="col-lg-5">
										<div class="box_general">
											<div class="form-group">
												<input type="text" name="db_host" class=" form-control" placeholder="Database Host" required>
											</div>
											<div class="form-group">
												<input type="text" name="db_user" class=" form-control" placeholder="Database User" required
											</div>
											<div class="form-group">
												<input type="text" name="db_password" class=" form-control" placeholder="Database Password"required>
											</div>
											<div class="form-group">
												<input type="text" name="db_name" class=" form-control" placeholder="Database Name" required>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="bottom-wizard">
						<button class="btn btn-info anchor">Next Step <i class="fa fa-angle-double-right"></i></button>
					</div>
				</form>
			</div>
		</div>										
<?php $__env->stopSection(); ?>
<?php echo $__env->make('install.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>