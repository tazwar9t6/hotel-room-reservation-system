<?php $__env->startSection('content'); ?>
<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Home</a></li>
				<li><a href="javascript:;">Room Management</a></li>
				<li class="active">All Room</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Room Management <button type="button" data-toggle="modal" data-target="#addRoom" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New Room</button> <button type="button" data-toggle="modal" data-target="#addRoomtype" class="btn btn-inverse"><i class="fa fa-plus"></i> Add New Room Type</button> <a type="button" href="<?php echo e(route('roomtypes')); ?>" class="btn btn-inverse"><i class="fa fa-eye"></i> Show Room Types</a></h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">All Rooms</h4>
                        </div>
                        <div class="panel-body">
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Room Number</th>
                                        <th>Room Type</th>
                                        <th>Booking Status</th>
                                        <th>Check In</th>
                                        <th>Check Out</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
								<?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php
								$room_type = \App\Roomtype::where('id',$room->room_type_id)->first();
								?>
                                        <td><b style="font-size:14px;"><?php echo e($room->room_no); ?></b></td>
                                        <td><b style="font-size:14px;"><?php echo e($room_type->room_type); ?></b></td>
                                        <td><?php if($room->status == 1): ?>
										<button type="button" class="btn btn-danger m-r-5 m-b-5">Booked</button>
									    <?php else: ?>
										<a type="submit" href="<?php echo e(route('book', [$room->room_type_id, $room->id])); ?>" class="btn btn-success m-r-5 m-b-5">Book</a>
									    <?php endif; ?>
										</td>
                                        <td><?php if($room->status == 1 && $room->check_in_status == 1): ?>
										<button type="button" class="btn btn-danger m-r-5 m-b-5">Checked In</button>
									    <?php elseif($room->status == 1 && $room->check_in_status == 0): ?>
									    <button type="button" id="checkInRoom"  data-id="<?php echo e($room->id); ?>" data-toggle="modal" data-target="#checkIn" class="btn btn-success m-r-5 m-b-5">Check In</button>
									    <?php endif; ?>
									    </td>
                                        <td><?php if($room->status == 1 && $room->check_in_status == 1): ?>
										<button type="button" id="checkOutRoom" data-id="<?php echo e($room->id); ?>" class="btn btn-success m-r-5 m-b-5">Check Out</button>
									    <?php endif; ?>
									    </td>
										<td><?php if($room->status == 1): ?>
											<a data-toggle="modal" data-target="#cutomerDetail" data-id="<?php echo e($room->id); ?>" id="cutomerDetails" class="btn btn-info btn-icon btn-circle btn-lg"><i class="fa fa-eye"></i></a>
										<?php endif; ?>
										<a data-toggle="modal" data-target="#edit<?php echo e($room->id); ?>" class="btn btn-success btn-icon btn-circle btn-lg"><i class="fa fa-pencil"></i></a>
										<a onclick="confirm_click<?php echo e($room->id); ?>();" class="btn btn-danger btn-icon btn-circle btn-lg"><i class="fa fa-trash"></i></a>
		                                <script type="text/javascript">
										function confirm_click<?php echo e($room->id); ?>()
										{
											if (confirm("Are you sure?")) {
												event.preventDefault();
												document.getElementById('delete-form<?php echo e($room->id); ?>').submit();
												} else {
													return false;
													}
											}
											</script>
										<form id="delete-form<?php echo e($room->id); ?>" action="<?php echo e(route('removeroom')); ?>" method="post" style="display: none;">
										<?php echo e(csrf_field()); ?>

										<input type="hidden" name="id" value="<?php echo e($room->id); ?>">
										</form>
										</td>
										
										
                                    </tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
			
			    <!-- Add Room Modal -->
    <div id="addRoom" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Room</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('addroom')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

                                <div class="response"></div>
                                <div class="form-group">
                                    <label>Room Type</label>
                                    <select class="form-control" name="roomtype" required
                                            data-error="Select Room Type">
                                        <option selected disabled>Select Room Type</option>
										<?php
										$room_types = \App\Roomtype::all();
										?>
										<?php $__currentLoopData = $room_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($type->id); ?>"><?php echo e($type->room_type); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label>Room Number</label>
                                    <input class="form-control" placeholder="Room No" name="roomnumber"
                                           data-error="Enter Room No" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <button class="btn btn-success pull-right">Add Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
			
				    <!-- Add Room Type -->
    <div id="addRoomtype" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Room Type</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('addroomtype')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

                                <div class="response"></div>
                                <div class="form-group">
                                    <label>Room Type</label>
									<input class="form-control" placeholder="Room type" name="roomtype"
                                           data-error="Enter Room type" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label>Room Price</label>
                                    <input class="form-control" placeholder="Room Price" name="roomprice"
                                           data-error="Enter Room Price" required>
                                    <div class="help-block with-errors"></div>
                                </div>
								
								<div class="form-group">
                                    <label>Max Person</label>
                                    <input class="form-control" placeholder="Max Person" name="maxperson"
                                           data-error="Enter Max Person" required>
                                    <div class="help-block with-errors"></div>
                                </div>
								
								
                                <button class="btn btn-success pull-right">Add Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
			
			
<?php $__currentLoopData = $rooms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<div id="edit<?php echo e($room->id); ?>" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Room</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form action="<?php echo e(route('editroom')); ?>" data-toggle="validator" method="post" role="form">
							<?php echo e(csrf_field()); ?>

							<input type="hidden" name="id" value="<?php echo e($room->id); ?>">
                                <div class="response"></div>
                                <div class="form-group">
                                    <label>Room Type</label>
                                    <select class="form-control" name="room_type_id" required
                                            data-error="Select Room Type">
                                        <option selected disabled>Select Room Type</option>
										<?php
										$room_types = \App\Roomtype::all();
										?>
										<?php $__currentLoopData = $room_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<option value="<?php echo e($type->id); ?>"<?php if($type->id == $room->room_type_id): ?> selected <?php endif; ?> ><?php echo e($type->room_type); ?></option>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
										</select>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label>Room Number</label>
                                    <input class="form-control" placeholder="Room Number" value="<?php echo e($room->room_no); ?>" name="room_no"
                                           data-error="Enter Price" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <button class="btn btn-success pull-right">Update Room</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
			
			
    <div id="checkIn" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Check In Room</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table style="font-size:13px;font-weight: bold;" class="table table-responsive table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td id="getCustomerName"></td>
                                </tr>
                                <tr>
                                    <td>Room Type</td>
                                    <td id="getRoomType"></td>
                                </tr>
								<tr>
                                    <td>Room Per Day Cost</td>
                                    <td id="getRoomPrice"></td>
                                </tr>
                                <tr>
                                    <td>Room No</td>
                                    <td id="getRoomNo"></td>
                                </tr>
                                <tr>
                                    <td>Check In</td>
                                    <td id="getCheckIn"></td>
                                </tr>
                                <tr>
                                    <td>Check Out</td>
                                    <td id="getCheckOut"></td>
                                </tr>
                                <tr>
                                    <td>Total Price</td>
                                    <td id="getTotalPrice"></td>
                                </tr>
                                </tbody>
                            </table>
                            <form role="form" id="advancePayment">
                                <div class="form-group col-lg-12">
                                    <label><b>Advance Payment</b></label>
                                    <input type="number" class="form-control" id="advance_payment"
                                           placeholder="Advance Payment">
                                </div>
                                <input type="hidden" id="getBookingID" value="">
                                <button type="submit" class="btn btn-success pull-right">Payment & Check In</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	
	   <div id="cutomerDetail" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Customer Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table style="font-size:13px;font-weight: bold;" class="table table-responsive table-bordered">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td id="customer_name"></td>
                                </tr>
                                <tr>
                                    <td>Contact Number</td>
                                    <td id="customer_contact_no"></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td id="customer_email"></td>
                                </tr>
                                <tr>
                                    <td>ID Card Type</td>
                                    <td id="customer_id_card_type"></td>
                                </tr>
                                <tr>
                                    <td>ID Card Number</td>
                                    <td id="customer_id_card_number"></td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td id="customer_address"></td>
                                </tr>
								<tr>
                                    <td>Booking date</td>
                                    <td id="book_date"></td>
                                </tr>
								<tr>
                                    <td>Checked In</td>
                                    <td id="checked_in"></td>
                                </tr>
                                <tr>
                                    <td>Remaining Amount</td>
                                    <td id="remaining_price"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
				<div class="modal-footer">
											<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
										</div>
            </div>
        </div>
    </div>
	    <div id="checkOut" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Check Out Room</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table style="font-size:13px;font-weight: bold;" class="table table-responsive table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td id="getCustomerName_n"></td>
                                </tr>
                                <tr>
                                    <td>Room Type</td>
                                    <td id="getRoomType_n"></td>
                                </tr>
                                <tr>
                                    <td>Room No</td>
                                    <td id="getRoomNo_n"></td>
                                </tr>
                                <tr>
                                    <td>Check In</td>
                                    <td id="getCheckIn_n"></td>
                                </tr>
                                <tr>
                                    <td>Check Out</td>
                                    <td id="getCheckOut_n"></td>
                                </tr>
                                <tr>
                                    <td>Total Day</td>
                                    <td id="getTotalDay_n"></td>
                                </tr>
                                <tr>
                                    <td>Total Hour</td>
                                    <td id="getTotalHour_n"></td>
                                </tr>
                                <tr>
                                    <td>Total Amount</td>
                                    <td id="getTotalPrice_n"></td>
                                </tr>
								<tr>
                                    <td>Paid In Advance</td>
                                    <td id="getAdvance_n"></td>
                                </tr>
                                <tr>
                                    <td>Remaining Amount</td>
                                    <td id="getRemainingPrice_n"></td>
                                </tr>
                                </tbody>
                            </table>
                            <form role="form" id="checkOutRoom_n">
                                <div class="form-group col-lg-12">
                                    <label>Remaining Payment</label>
                                    <input type="text" class="form-control" id="remaining_amount"
                                           placeholder="Remaining Payment" required>
                                </div>
                                <input type="hidden" id="getBookingId_n" value="">
                                <button type="submit" class="btn btn-success pull-right">Payment & Checkout</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
	
	
<script>
    var checkInRoom = "<?php echo e(route('checkInRoom')); ?>";
	var advancePayment = "<?php echo e(route('advancePayment')); ?>";
	var token = "<?php echo e(csrf_token()); ?>";
	var redirect_path = "<?php echo e(route('allroom')); ?>";
	var cutomerDetails = "<?php echo e(route('cutomerDetails')); ?>";
	var checkIntoday = "<?php echo e(route('checkIntoday')); ?>";
	var checkOutRoom = "<?php echo e(route('checkOutRoom')); ?>";
	var checkOuttoday = "<?php echo e(route('checkOuttoday')); ?>";
	var checkOutBooking = "<?php echo e(route('checkOutBooking')); ?>";
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('extracss'); ?>
<link href="<?php echo e(asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrajs'); ?>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/jquery.dataTables.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/table-manage-default.demo.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/ajax.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('extrainit'); ?>
TableManageDefault.init();
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>