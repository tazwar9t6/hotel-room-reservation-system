$(document).on('click', '#checkInRoom', function (e) {
    e.preventDefault();

    var room_id = $(this).data('id');

    $.ajax({
        type: 'post',
        url: checkInRoom,
        dataType: 'JSON',
        data: {
            id: room_id,
            _token: token
        },
        success: function (response) {
                $('#room_id').val(room_id);
                $('#getCustomerName').html(response.name);
                $('#getRoomType').html(response.room_type);
                $('#getRoomNo').html(response.room_no);
                $('#getCheckIn').html(response.check_in);
                $('#getCheckOut').html(response.check_out);
                $('#getTotalPrice').html(response.total_price + '/-');
				$('#getRoomPrice').html(response.room_price + '/-');
                $('#getBookingID').val(response.booking_id);
                $('#checkIn').modal('show');
        },
      error:function (error) {
			$.gritter.add({
            title: "Unknown error",
            text: 'Please contact with developer.',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
      }
    });

});

$('#advancePayment').submit(function () {

    var booking_id = $('#getBookingID').val();
    var advance_payment = $('#advance_payment').val();

    $.ajax({
        type: 'post',
        url: advancePayment,
        dataType: 'JSON',
        data: {
            booking_id: booking_id,
            advance_payment: advance_payment,
            _token: token
        },
        success: function (response) {
                $('#checkIn').modal('hide');
                $.gritter.add({
            title: "Successfully Updated",
            text: 'Room Has been checked In.',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		setTimeout(function(){location.href= redirect_path } , 3000);   
        }
    });

    return false;
});

$(document).on('click', '#cutomerDetails', function (e) {
    e.preventDefault();
    var room_id = $(this).data('id');
    $.ajax({
        type: 'post',
        url: cutomerDetails,
        dataType: 'JSON',
        data: {
            room_id: room_id,
			_token: token
        },
        success: function (response) {

                $('#customer_name').html(response.customer_name);
                $('#customer_contact_no').html(response.contact_no);
                $('#customer_email').html(response.email);
                $('#customer_id_card_type').html(response.id_card_type);
                $('#customer_id_card_number').html(response.id_card_no);
                $('#customer_address').html(response.address);
				$('#book_date').html(response.book_date);
                $('#checked_in').html(response.checked_in);
                $('#remaining_price').html(response.remaining_price);
        }
    });

});

$(document).on('click', '#usreDetails', function (e) {
    e.preventDefault();
    var user_id = $(this).data('id');
    $.ajax({
        type: 'post',
        url: usreDetails,
        dataType: 'JSON',
        data: {
            id: user_id,
			_token: token
        },
        success: function (response) {

                $('#customer_name').html(response.customer_name);
                $('#customer_contact_no').html(response.contact_no);
                $('#customer_email').html(response.email);
                $('#customer_id_card_type').html(response.id_card_type);
                $('#customer_id_card_number').html(response.id_card_no);
                $('#customer_address').html(response.address);
				$('#booking_date').html(response.booking_date);
                $('#check_id').html(response.check_id);
                $('#check_out').html(response.check_out);
				$('#total').html(response.total);
				$('#advance').html(response.advance);
				$('#status').html(response.payment_status);
				
        }
    });

});

$(document).on('click', '#checkOutRoom', function (e) {
    e.preventDefault();
    var room_id = $(this).data('id');
    $.ajax({
        type: 'post',
        url: checkOutRoom,
        dataType: 'JSON',
        data: {
            id: room_id,
            _token: token
        },
        success: function (response) {
                $('#getCustomerName_n').html(response.name);
                $('#getRoomType_n').html(response.room_type);
                $('#getRoomNo_n').html(response.room_no);
                $('#getCheckIn_n').html(response.check_in);
                $('#getCheckOut_n').html(response.check_out);
				$('#getTotalDay_n').html(response.day);
			    $('#getTotalHour_n').html(response.hour);
                $('#getTotalPrice_n').html(response.total_price + ' /-');
				$('#getAdvance_n').html(response.paid + ' /-');
                $('#getRemainingPrice_n').html(response.remaining_price + ' /-');
                $('#getBookingId_n').val(response.booking_id);
                $('#checkOut').modal('show');
        }
    });

});

$('#checkOutRoom_n').submit(function (e) {
	if(e.keyCode == 13)
	{
      return false;
	  }
	
    var booking_id = $('#getBookingId_n').val();
    var remaining_amount = $('#remaining_amount').val();
    var payment = document.getElementById("getRemainingPrice_n").innerText;
	var pay = payment.match(/\d/g);
	pay = pay.join("");
	
	if(payment.indexOf('Return') > -1){
		remaining_amount = "null";
	}
	
	if ( remaining_amount < pay && !(payment.indexOf('Return') > -1))
	{
	if (confirm("Do you want to continue process without full payment ?")) {
    $.ajax({
        type: 'post',
        url: checkOutBooking,
        dataType: 'JSON',
        data: {
            booking_id: booking_id,
            remaining_amount: remaining_amount,
            _token: token
        },
        success: function (response) {
			$('#checkOut').modal('hide');
            $.gritter.add({
            title: "Payment Updated",
            text: 'Please wait...',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		setTimeout(function(){location.href= response.url } , 1000);  
        },
		error:function (error) {
			$.gritter.add({
            title: "Payment error",
            text: 'Please enter the full payment.',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
      }
    });
	}
	}
	else
	{
		$.ajax({
        type: 'post',
        url: checkOutBooking,
        dataType: 'JSON',
        data: {
            booking_id: booking_id,
            remaining_amount: remaining_amount,
            _token: token
        },
        success: function (response) {
			$('#checkOut').modal('hide');
            $.gritter.add({
            title: "Payment Updated",
            text: 'Please wait...',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		setTimeout(function(){location.href= response.url } , 1000);  
        },
		error:function (error) {
			$.gritter.add({
            title: "Payment error",
            text: 'Please enter the full payment.',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
      }
    });
	}
	return false;
});

$(document).on('click', '#checkIntoday', function (e) {
    e.preventDefault();
	if (confirm("Are you sure?")) {
    var id = $(this).data('id');
    $.ajax({
        type: 'post',
        url: checkIntoday,
        dataType: 'JSON',
        data: {
            id: id,
			_token: token
        },
        success: function (response) {

                $('#getCheckIn').html(response.check_in);
				$('#getTotalPrice').html(response.total_price + '/-');
        }
    });
	} else {
	return false;
	}
});

$(document).on('click', '#checkOuttoday', function (e) {
    e.preventDefault();
	if (confirm("Are you sure?")) {
    var id = $(this).data('id');
    $.ajax({
        type: 'post',
        url: checkOuttoday,
        dataType: 'JSON',
        data: {
            id: id,
			_token: token
        },
        success: function (response) {
			    $('#getTotalDay_n').html(response.day);
			    $('#getTotalHour_n').html(response.hour);
                $('#getCheckOut_n').html(response.check_out);
                $('#getTotalPrice_n').html(response.total_price + '/-');
                $('#getRemainingPrice_n').html(response.remaining_price + '/-');
        }
    });
	} else {
	return false;
	}
});

