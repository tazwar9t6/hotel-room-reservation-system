var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var checkin = $('#check_in_date').fdatepicker({
    format: 'dd-mm-yyyy',
    onRender: function (date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
}).on('changeDate', function (ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        checkout.update(newDate);
    }
    checkin.hide();
    $('#check_out_date')[0].focus();
}).data('datepicker');
var checkout = $('#check_out_date').fdatepicker({
    format: 'dd-mm-yyyy',
    onRender: function (date) {
        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
    }
}).on('changeDate', function (ev) {
    checkout.hide();
    var totalDays = Math.floor((checkout.date - checkin.date)/86400000);
    var price = document.getElementById('price').innerHTML;
    var total_price = (totalDays+1)*(price);
    $('#staying_day').html(totalDays+1);
    $('#total_price').html(total_price);
}).data('datepicker');



var joining_date = $('.joining_date').fdatepicker({
    format: 'dd-mm-yyyy',
    onRender: function (date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
}).data('datepicker');



$(document).ready(function(){
	
	$(document).on('click','#submit',function(e){		
	e.preventDefault();	
	var $this = $(this);
	$this.button('loading');
    var customer = $('#customer').val();
    var room_type_id = $('#room_type').val();
    var room_type = $("#room_type :selected").text();
    var check_in_date = $('#check_in_date').val();
    var check_out_date = $('#check_out_date').val();
    var first_name = $('#first_name').val();
    var last_name = $('#last_name').val();
    var contact_no = $('#contact_no').val();
    var email = $('#email').val();
    var id_card_type = $('#id_card_type').val();
    var id_card_no = $('#id_card_no').val();
    var address = $('#address').val();
    var total_price = document.getElementById('total_price').innerHTML;
	

	if(!room_type_id){
		$.gritter.add({
            title: "Select room type",
            text: '<b><div style="font-size:10px;">Please select room type</div></b>',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		$this.button('reset');
    }
	else if(!check_in_date){
		$.gritter.add({
            title: "Select Check In Date",
            text: '<b><div style="font-size:10px;">Please fill Check In Date</div></b>',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		$this.button('reset');
    }
	else if(!check_out_date){
		$.gritter.add({
            title: "Select Check Out Date",
            text: '<b><div style="font-size:10px;">Please fill Check Out Date</div></b>',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		$this.button('reset');
    }
	else{
        $.ajax({
            type: 'post',
            url: booking,
            dataType: 'JSON',
            data: {
				_token:token,
                customer:customer,
                room_type_id:room_type_id,
                check_in:check_in_date,
                check_out:check_out_date,
                name:first_name+' '+last_name,
                contact_no:contact_no,
                email:email,
                id_card_type:id_card_type,
                id_card_no:id_card_no,
                address:address
            },
            success: function (response) {
                    $('#getCustomerName').html(first_name+' '+last_name);
                    $('#getRoomType').html(room_type);
                    $('#getCheckIn').html(check_in_date);
                    $('#getCheckOut').html(check_out_date);
                    $('#getTotalPrice').html(total_price);
                    $('#bookingConfirm').modal('show');
					$this.button('reset');
                    document.getElementById("booking").reset();
            },
      error:function (xhr) {
		 var message = JSON.parse(xhr.responseText);
		$.each(message.errors, function(key,value) {
			$.gritter.add({
            title: value,
            text: '<b><div style="font-size:10px;">Please check</div></b>',
            sticky: true,
            time: "",
            class_name: "my-sticky-class"
        });
		});
		$this.button('reset');
      }
        });
    }

    return false;
});
}); 

$(document).ready(function(){
  $("#customer").change(function(){
    var customer = $(this).val();
	
	if(customer == "new")
	{
		$("#old_customer").hide();
		$("#customer_details").show();		
	}
	if(customer == "old")
	{
		$("#old_customer").show();
		$("#customer_details").hide();		
	}
  });
});
