-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2019 at 11:34 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel_test1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Winter', 'moh4mmadsakib@gmail.com', 'admin', '$2y$10$zbwM4OAO6rVPHT1xZfgpU.G5Y3nZPsbypnAzJ/62WYcpCK4RdIefG', 'lnTk38Z1ZEtebsGo2u5bjeLUwvwrYEp8NPBc9ULQBtNT8PJRZn9UmImFCbtp', 'admin', '2018-08-09 19:10:38', '2018-08-09 19:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `room_id` int(10) NOT NULL,
  `booking_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `check_in` varchar(100) DEFAULT NULL,
  `check_out` varchar(100) DEFAULT NULL,
  `total_price` int(10) NOT NULL,
  `advance` int(10) NOT NULL,
  `remaining_price` int(10) NOT NULL,
  `payment_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `customer_id`, `room_id`, `booking_date`, `check_in`, `check_out`, `total_price`, `advance`, `remaining_price`, `payment_status`, `created_at`, `updated_at`) VALUES
(2, 2, 3, '2018-08-09 19:49:42', '13-11-2018 17:35:29', '13-11-2018 17:36:02', -744000, -1, 0, 1, '2018-08-09 13:49:42', '2018-11-13 11:36:02'),
(3, 3, 2, '2018-11-13 17:37:09', '13-11-2018 17:37:26', '13-11-2018 17:41:56', 4000, 2, 3998, 1, '2018-11-13 11:37:09', '2018-11-13 11:41:56'),
(4, 4, 2, '2018-11-13 17:44:27', '13-11-2018 23:44:40', '13-11-2018 23:47:39', 4000, 2, 0, 1, '2018-11-13 17:44:27', '2018-11-13 17:47:39'),
(5, 5, 2, '2019-06-26 13:48:48', '24-06-2019 19:49:09', '26-06-2019 20:30:59', 8000, 0, 0, 1, '2019-06-26 13:48:48', '2019-06-26 14:30:59'),
(6, 6, 3, '2019-07-04 11:25:35', '04-07-2019 20:41:19', '04-07-2019 23:08:10', 8000, 0, 1000, 1, '2019-07-04 16:25:35', '2019-07-04 17:08:10'),
(7, 7, 2, '2019-07-04 16:43:12', '04-07-2019 22:58:38', '04-07-2019 23:07:14', 4000, 1000, 0, 1, '2019-07-04 16:43:12', '2019-07-04 17:07:14'),
(8, 8, 2, '2019-07-04 10:09:38', '04-07-2019 19:09:57', '04-07-2019 23:11:30', 4000, 500, -500, 1, '2019-07-04 17:09:38', '2019-07-04 17:11:30'),
(9, 9, 2, '2019-07-04 17:12:40', '04-07-2019 23:12:48', '04-07-2019 23:13:07', 4000, 500, -500, 1, '2019-07-04 17:12:40', '2019-07-04 17:13:07'),
(10, 10, 3, '2019-07-04 17:18:32', '04-07-2019 23:25:35', 'Jul 4, 2019 11:19 PM', 8000, 10000, 0, 1, '2019-07-04 17:18:32', '2019-07-04 17:19:56'),
(11, 11, 2, '2019-07-04 17:25:19', '04-07-2019 23:25:35', '04-07-2019 23:28:53', 8000, 500, 0, 1, '2019-07-04 17:25:19', '2019-07-04 17:28:53'),
(12, 14, 2, '2019-09-25 10:51:23', '25-09-2019 16:52:37', '25-09-2019 16:53:12', 4000, 0, 0, 1, '2019-09-25 10:51:23', '2019-09-25 10:53:12'),
(16, 6, 3, '2019-09-25 17:03:54', '25-09-2019 23:13:40', '25-09-2019 23:13:55', 32000, 0, 0, 1, '2019-09-25 17:03:54', '2019-09-25 17:13:55'),
(17, 12, 2, '2019-09-25 17:04:30', '25-09-2019 23:06:42', '25-09-2019 23:07:00', 8000, 0, 0, 1, '2019-09-25 17:04:30', '2019-09-25 17:07:00'),
(18, 20, 4, '2019-09-25 17:05:19', '25-09-2019', '26-09-2019', 16000, 0, 16000, 0, '2019-09-25 17:05:19', '2019-09-25 17:05:19'),
(19, 15, 5, '2019-09-25 17:08:14', '25-09-2019', '27-09-2019', 24000, 0, 24000, 0, '2019-09-25 17:08:14', '2019-09-25 17:08:14'),
(20, 15, 2, '2019-09-25 17:09:59', '25-09-2019', '27-09-2019', 12000, 0, 12000, 0, '2019-09-25 17:09:59', '2019-09-25 17:09:59'),
(21, 15, 3, '2019-09-25 17:14:13', '01-12-2019', '04-12-2019', 32000, 0, 32000, 0, '2019-09-25 17:14:13', '2019-09-25 17:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `watermark` varchar(255) NOT NULL,
  `footer` varchar(599) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `address`, `email`, `phone`, `currency`, `watermark`, `footer`, `created_at`, `updated_at`) VALUES
(1, 'Hotel Management', '455 Foggy Heights,<br>AZ 85004, US', 'hotel@hotel.com', '000000000', 'BDT', 'Paid', 'Invoice was created on a computer and is valid without the signature and seal.', NULL, '2019-07-04 17:18:48');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `contact_no` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `id_card_type_id` int(10) NOT NULL,
  `id_card_no` varchar(20) NOT NULL,
  `address` varchar(300) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `customer_name`, `contact_no`, `email`, `id_card_type_id`, `id_card_no`, `address`, `created_at`, `updated_at`) VALUES
(2, 'Alex Winter', 175496768, 'alex@test.co', 3, '8897896', '82 West Lane\nhiya gub', '2018-08-09 13:49:42', '2018-08-09 13:49:42'),
(3, 'Alice L Cross', 3, 'rabiul@iname.com', 2, '00000000000', 'h,,,', '2018-11-13 11:37:09', '2018-11-13 11:37:09'),
(4, 'Alice L Cross', 714, 'fcross@socal.rr.com', 3, '988970', '9431 Woodcrest Dr', '2018-11-13 17:44:27', '2018-11-13 17:44:27'),
(5, 'Alex Winter', 7149685, 'sk@dd.com', 1, '0980980', '33 Davids Lane', '2019-06-26 13:48:48', '2019-06-26 13:48:48'),
(6, 'Idris Ali Khan', 7865443322, 'idris@try.check', 1, '987878786767687678', '15 Golden Knowes Road\nhiya gub', '2019-07-04 16:25:34', '2019-09-25 16:51:16'),
(7, 'Alex Winter', 9809, 'sk@dd.com', 2, '090980', '33 Davids Lane', '2019-07-04 16:43:12', '2019-07-04 16:43:12'),
(8, 'Alex Winter', 8768679, 'sk@dd.com', 3, '87676576090', '33 Davids Lane', '2019-07-04 17:09:38', '2019-07-04 17:09:38'),
(9, 'cc Security', 9879087, 'khill.meeh@gmail.com', 3, '987908', '15 Golden Knowes Road\nhiya gub', '2019-07-04 17:12:40', '2019-07-04 17:12:40'),
(10, 'cc Security', 98908, 'khill.meeh@gmail.com', 3, '987', '15 Golden Knowes Road\nhiya gub', '2019-07-04 17:18:32', '2019-07-04 17:18:32'),
(11, 'echoo mi aka', 989089, 'sk@dd.com', 1, '87678', '3449 Desert Broom Court', '2019-07-04 17:25:19', '2019-07-04 17:25:19'),
(12, 'Alex Winter', 714968557609098, 'sk@dd.com', 1, '777878', '33 Davids Lane', '2019-09-24 06:42:04', '2019-09-24 06:42:04'),
(13, 'Sk mahmud', 9989887, 'sk@dd.com', 3, '88789', '3449 Desert Broom Court', '2019-09-25 09:38:27', '2019-09-25 09:38:27'),
(14, 'Sk mahmud', 188812, 'sk@dd.com', 1, '09', '3449 Desert Broom Court', '2019-09-25 10:51:20', '2019-09-25 10:51:20'),
(15, 'Alex Winter', 7149685576, 'rabiul@iname.com', 3, '879899', '33 Davids Lane', '2019-09-25 10:55:35', '2019-09-25 10:55:35'),
(20, 'mohammad sakib', 1754958684, 'moh4mmadsakib@gmail.com', 2, '98789', 'chittagong\nchittagong', '2019-09-25 17:05:19', '2019-09-25 17:05:19');

-- --------------------------------------------------------

--
-- Table structure for table `emp_history`
--

CREATE TABLE `emp_history` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `from_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `to_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `id_card_type`
--

CREATE TABLE `id_card_type` (
  `id` int(10) NOT NULL,
  `id_card_type` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_card_type`
--

INSERT INTO `id_card_type` (`id`, `id_card_type`, `created_at`, `updated_at`) VALUES
(1, 'Nation ID Card', NULL, NULL),
(2, 'Voter ID Card', NULL, NULL),
(3, 'Passport', NULL, '2018-05-01 17:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `request_booking`
--

CREATE TABLE `request_booking` (
  `id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `room_type` int(10) NOT NULL,
  `booking_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `check_in` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `check_out` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_booking`
--

INSERT INTO `request_booking` (`id`, `customer_id`, `room_type`, `booking_date`, `check_in`, `check_out`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 2, '2019-09-24 06:42:04', '24-09-2019', '25-09-2019', 0, '2019-09-24 06:42:04', '2019-09-25 17:04:30'),
(3, 15, 4, '2019-09-25 10:55:35', '25-09-2019', '27-09-2019', 0, '2019-09-25 10:55:35', '2019-09-25 17:08:14'),
(4, 6, 4, '2019-09-25 14:38:29', '25-09-2019', '28-09-2019', 0, '2019-09-25 14:38:29', '2019-09-25 17:03:54'),
(5, 15, 4, '2019-09-25 17:13:25', '01-12-2019', '04-12-2019', 0, '2019-09-25 17:13:25', '2019-09-25 17:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(10) NOT NULL,
  `room_type_id` int(10) NOT NULL,
  `room_no` varchar(10) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `check_in_status` tinyint(1) NOT NULL,
  `check_out_status` tinyint(1) NOT NULL,
  `deleteStatus` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `room_type_id`, `room_no`, `status`, `check_in_status`, `check_out_status`, `deleteStatus`, `created_at`, `updated_at`) VALUES
(2, 2, '124', 1, 0, 1, 0, '2018-08-09 13:48:42', '2019-09-25 17:09:59'),
(3, 4, '999', 1, 0, 1, 0, '2018-08-09 13:48:48', '2019-09-25 17:14:13'),
(4, 4, '556', 1, 0, 0, 0, '2019-09-25 17:05:02', '2019-09-25 17:05:20'),
(5, 4, '550', 1, 0, 0, 0, '2019-09-25 17:07:33', '2019-09-25 17:08:14');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `id` int(10) NOT NULL,
  `room_type` varchar(100) NOT NULL,
  `price` int(10) NOT NULL,
  `max_person` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `room_type`, `price`, `max_person`, `created_at`, `updated_at`) VALUES
(2, 'Single', 4000, 2, NULL, '2018-04-21 08:06:10'),
(4, 'Double', 8000, 4, '2018-04-21 06:40:49', '2018-04-21 08:06:02');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id` int(10) NOT NULL,
  `shift` varchar(100) NOT NULL,
  `shift_timing` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `shift`, `shift_timing`, `created_at`, `updated_at`) VALUES
(1, 'Night', '7 PM to 8 AM', '2018-03-31 18:00:00', '2018-04-30 12:35:07'),
(2, 'Day', '9 AM to 2 PM', NULL, NULL),
(3, 'Midnight', '2 PM TO 2 AM', '2018-04-28 17:59:31', '2018-04-28 17:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `smtp`
--

CREATE TABLE `smtp` (
  `id` int(11) NOT NULL,
  `smtp` varchar(255) NOT NULL,
  `smtp_host` varchar(255) NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_pass` varchar(255) NOT NULL,
  `smtp_port` varchar(255) NOT NULL,
  `smtp_secure` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `smtp`
--

INSERT INTO `smtp` (`id`, `smtp`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_secure`, `created_at`, `updated_at`) VALUES
(1, '1', 'smtp.gmail.com', '', '', '587', 'tls', NULL, '2018-04-14 12:48:28');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `staff_type_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `id_card_type` int(11) NOT NULL,
  `id_card_no` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact_no` bigint(20) NOT NULL,
  `salary` bigint(20) NOT NULL,
  `joining_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_type`
--

CREATE TABLE `staff_type` (
  `id` int(10) NOT NULL,
  `staff_type` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `room_id` (`room_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `contact_no` (`contact_no`),
  ADD KEY `id_card_type_id` (`id_card_type_id`);

--
-- Indexes for table `emp_history`
--
ALTER TABLE `emp_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `id_card_type`
--
ALTER TABLE `id_card_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_booking`
--
ALTER TABLE `request_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `room_type` (`room_type`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_type_id` (`room_type_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smtp`
--
ALTER TABLE `smtp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_card_type` (`id_card_type`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `staff_type_id` (`staff_type_id`);

--
-- Indexes for table `staff_type`
--
ALTER TABLE `staff_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `emp_history`
--
ALTER TABLE `emp_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `id_card_type`
--
ALTER TABLE `id_card_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `request_booking`
--
ALTER TABLE `request_booking`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `smtp`
--
ALTER TABLE `smtp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_type`
--
ALTER TABLE `staff_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`);

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`id_card_type_id`) REFERENCES `id_card_type` (`id`);

--
-- Constraints for table `request_booking`
--
ALTER TABLE `request_booking`
  ADD CONSTRAINT `request_booking_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `request_booking_ibfk_2` FOREIGN KEY (`room_type`) REFERENCES `room_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
